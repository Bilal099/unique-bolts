<!doctype html>
<html lang="en-US">

<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <title>Reset Password Email Template</title>
    <meta name="description" content="Reset Password Email Template.">
    <style type="text/css">
        a:hover {
            text-decoration: underline !important;
        }
    </style>
</head>

<body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #011321;" leftmargin="0">
    <!--100% body table-->
    <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#011321"
        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
        <tr>
            <td>
                <table style="background-color: #011321; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <a href="https://rakeshmandal.com" title="logo" target="_blank">
                                {{-- <img width="60" src="https://planets01.com/vtrade/img/logo.png" title="logo" alt="logo"> --}}


                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>

                                
                                <tr>
                                    <td style="padding:0 35px;">
                                        <h1
                                            style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:'Rubik',sans-serif;">
                                            Customer Contact Detail</h1>
                                        <span
                                            style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                        <br>
                                        <div style="display: inline-block;">
                                            <label> <strong> Name:</strong></label>
                                            <span>
                                            {{$contactUs->name}}

                                            </span>
                                        </div>
<br>
                                        <div style="display: inline-block;">
                                            <label> <strong> Email:</strong></label>
                                            <span>
                                            {{$contactUs->email}}
                                            </span>
                                        </div>

                                        
                                        <h4 for="">Message:</h4>
                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                            {{$contactUs->message}}
                                            {{-- Contrary to
                                            popular belief, Lorem Ipsum is not simply random text. It has roots in a
                                            piece of classical Latin literature from 45 BC, making it over 2000 years
                                            old. Richard McClintock, a Latin professor at Hampden-Sydney College in
                                            Virginia, looked up one of the more obscure Latin words, consectetur, from a
                                            Lorem Ipsum passage, and going through the cites of the word in classical
                                            literature, discovered the undoubtable source. Lorem Ipsum comes from
                                            sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The
                                            Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a
                                            treatise on the theory of ethics, very popular during the Renaissance. The
                                            first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line
                                            in section 1.10.32.

                                            The standard chunk of Lorem Ipsum used since the 1500s is reproduced below
                                            for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum
                                            et Malorum" by Cicero are also reproduced in their exact original form,
                                            accompanied by English versions from the 1914 translation by H. Rackham. --}}
                                        </p>


                                    </td>
                                </tr>
                                
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" style="padding-bottom:0;padding-right:0;padding-left:0;padding-top:0px;"
                            valign="middle">
                            <span class="sg-image"
                                data-imagelibrary="%7B%22width%22%3A%228%22%2C%22height%22%3A18%2C%22alt_text%22%3A%22Facebook%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/0a1d076f825eb13bd17a878618a1f749835853a3a3cce49111ac7f18255f10173ecf06d2b5bd711d6207fbade2a3779328e63e26a3bfea5fe07bf7355823567d.png%22%2C%22link%22%3A%22%23%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D">
                                <a href="https://www.facebook.com/vervewine/" target="_blank"><img alt="Facebook"
                                        height="18"
                                        src="https://marketing-image-production.s3.amazonaws.com/uploads/0a1d076f825eb13bd17a878618a1f749835853a3a3cce49111ac7f18255f10173ecf06d2b5bd711d6207fbade2a3779328e63e26a3bfea5fe07bf7355823567d.png"
                                        style="border-width: 0px; margin-right: 21px; margin-left: 21px; width: 8px; height: 18px;"
                                        width="8"></a>
                            </span>
                            <!--[if gte mso 9]>&nbsp;&nbsp;&nbsp;<![endif]-->
                            <span class="sg-image"
                                data-imagelibrary="%7B%22width%22%3A%2223%22%2C%22height%22%3A18%2C%22alt_text%22%3A%22Twitter%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/6234335b200b187dda8644356bbf58d946eefadae92852cca49fea227cf169f44902dbf1698326466ef192bf122aa943d61bc5b092d06e6a940add1368d7fb71.png%22%2C%22link%22%3A%22%23%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D">
                                <a href="https://twitter.com/vervewine" target="_blank"><img alt="Twitter" height="18"
                                        src="https://marketing-image-production.s3.amazonaws.com/uploads/6234335b200b187dda8644356bbf58d946eefadae92852cca49fea227cf169f44902dbf1698326466ef192bf122aa943d61bc5b092d06e6a940add1368d7fb71.png"
                                        style="border-width: 0px; margin-right: 16px; margin-left: 16px; width: 23px; height: 18px;"
                                        width="23"></a>
                            </span>
                            <!--[if gte mso 9]>&nbsp;&nbsp;&nbsp;&nbsp;<![endif]-->
                            <span class="sg-image"
                                data-imagelibrary="%7B%22width%22%3A%2218%22%2C%22height%22%3A18%2C%22alt_text%22%3A%22Instagram%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/uploads/650ae3aa9987d91a188878413209c1d8d9b15d7d78854f0c65af44cab64e6c847fd576f673ebef2b04e5a321dc4fed51160661f72724f1b8df8d20baff80c46a.png%22%2C%22link%22%3A%22%23%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D">
                                <a href="https://www.instagram.com/vervewine/" target="_blank"><img alt="Instagram"
                                        height="18"
                                        src="https://marketing-image-production.s3.amazonaws.com/uploads/650ae3aa9987d91a188878413209c1d8d9b15d7d78854f0c65af44cab64e6c847fd576f673ebef2b04e5a321dc4fed51160661f72724f1b8df8d20baff80c46a.png"
                                        style="border-width: 0px; margin-right: 16px; margin-left: 16px; width: 18px; height: 18px;"
                                        width="18"></a>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p
                                style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">
                                &copy; <strong>Your Site Link</strong></p>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:80px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!--/100% body table-->
</body>

</html>