@extends('admin.layouts.app')

@push('custom-css')
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.css"
    integrity="sha512-gX6K9e/4ewXjtn8Q/oePzgIxs2KPrksR4S2NNMYLxenvF7n7eNon9XbqQxb+5jcqYBVCcncIxqF6fXJYgQtoAg=="
    crossorigin="anonymous" />
<style>
    .text-limit {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        max-width: 150px;
    }

    .center {
        display: flex;
        justify-content: center;
        align-items: center;
    }

</style>
@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Add Product</h3>
                        </div>

                        {{-- <div class="" style="display: none" id="msg_alert">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> <span id="ajax_msg_title">Success!</span> </h5>
                            <p id="ajax_msg"></p>
                        </div> --}}
                        @if (Session::has('success'))
                        <div class="alert alert-info alert-dismissible m-3">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> Success!</h5>
                            {{Session::get('success')}}
                        </div>
                        @elseif (Session::has('error'))
                        <div class="alert alert-danger alert-dismissible m-3">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> Error!</h5>
                            {{Session::get('error')}}
                        </div>
                        @endif
                        <div class="card-body">
                           
                            <form action="{{route('admin.productStore')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                @if (isset($data->id))
                                    <input type="hidden" name="id" value="{{ @$data->id }}">
                                @endif
                                <div class="card-body row">
                                    <div class="form-group col-md-6">
                                        <label for="">Product Name<span style="color: red">*</span></label>
                                        <input type="text" name="name"
                                            class="form-control  @error('name') is-invalid @enderror"
                                            value="{{(old('name')!=null)? (old('name')):(isset($data->name)? $data->name:'')}}">
                                        @error('name')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="">Select Category<span style="color: red">*</span></label>
                                        <select name="category_id" id="" class="form-control  @error('category_id') is-invalid @enderror">
                                            <option value="0" disabled selected>Select Option</option>
                                            @foreach ($category as $item)
                                                <option value="{{$item->id}}" {{ (isset($data->category_id)? (($data->category_id == $item->id)? "selected":"" ):"" ) }} >{{$item->name}} {{($item->parent_id!=null)? "/ ".$item->parent->name:"" }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    {{-- <div class="form-group col-md-6">
                                        <label for="">Price<span style="color: red"></span></label>
                                        <input type="number" name="price" min="0"
                                            class="form-control  @error('price') is-invalid @enderror" step="0.01"
                                            value="{{(old('price')!=null)? (old('price')):(isset($data->price)? $data->price:'')}}">
                                        @error('price')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div> --}}

                                    <div class="form-group col-md-6">
                                        <label for="">Image</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" accept="image/*" name="image" class="custom-file-input @error('image') is-invalid @enderror">
                                                <label class="custom-file-label" for="">Choose file</label>
                                            </div>
                                        </div>
                                        @error('image')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                    @if (isset($data->image))
                                    <div class="form-group col-md-6">
                                        <img src="{{ @asset('backend/images/product')."/".((@$data->image!="")? @$data->image:"dummy_image.png")}}" height="150" width="auto" alt="" srcset="">
                                    </div>
                                    @endif

                                    {{-- <div class="form-group col-md-6">
                                        <label for="">Description<span style="color: red"></span></label>
                                            <textarea  class="form-control  @error('description') is-invalid @enderror" name="description" id="" cols="30" rows="5">{{(old('description')!=null)? (old('description')):(isset($data->description)? $data->description:'')}}</textarea>
                                        @error('description')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div> --}}

                                    <div class="form-group col-md-12">
                                        <label for="">Description <span style="color: red">*</span> </label>
                                        <textarea id="summernote" name="description"
                                            class="form-control @error('description') is-invalid @enderror"
                                            cols="30"
                                            rows="10">{{(old('description')!=null)? (old('description')):(isset($data->description)? $data->description:'')}}</textarea>
                                        @error('description')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>

                                    
                                    

                                    <div class="form-group col-md-12">
                                        <label for="album">Upload Album</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="album[]" id="album" multiple {{  (isset($data->CompanyImage)? ((count($data->CompanyImage)>=2)?'disabled':""):"" ) }}>
                                                <label class="custom-file-label" for="album">Choose Pictures</label>
                                            </div>
                                        </div>
                                        @error('album')
                                            <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>

                                    @if (isset($data->ProductImage))
                                        @if (count($data->ProductImage) > 0)       
                                            <div class="form-group col-md-6" style="margin: 0 auto">
                                                
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                Upload Album Images
                                                            </th>
                                                            <th>
                                                                Action
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($data->ProductImage as $item)
                                                        <tr>
                                                            <td>
                                                                {{-- <img src="{{"/storage/companyImages/".((@$item->image!="")? @$item->image:"dummy_image.png")}}" height="150" width="auto" alt="" srcset="">      --}}
                                                                <img src="{{ @asset('backend/images/productImage')."/".((@$item->image!="")? @$item->image:"dummy_image.png")}}" height="150" width="auto" alt="" srcset="">     
                                                                <input type="hidden" name="album_image_id" class="album_image_id" value="{{@$item->id}}">
                                                                <input type="hidden" name="album_image_parent_id" class="album_image_parent_id" value="{{@$data->id}}">
                                                            </td>
                                                            <td style="text-align: center">
                                                                <button class="btn btn-danger album_image_delete">Delete</button>
                                                            </td>
                                                        </tr>
                                                        @endforeach

                                                    </tbody>
                                                </table>
                                                
                                            </div>
                                        @endif
                                    @endif

                                </div>
                                <button type="submit" class="btn btn-info">Submit</button>
                                <a href="{{route('admin.product')}}" class="btn btn-default">Back</a>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
        </div>

      

       

        
    </section>

</div>



@endsection


@push('custom-script')
<script src="{{asset('backend/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.js"></script>

<script>
    $(function () {
        $("#data-table").DataTable();
        $('#summernote').summernote();

    });

    $('.album_image_delete').click(function (e) {
        e.preventDefault();
        let _this = $(this);
        let id      = _this.closest('tr').find('.album_image_id').val();
        let parent_id = _this.closest('tr').find('.album_image_parent_id').val();

        Swal.fire({
            title: 'Are you sure, you want to Delete the selected record?',
            showDenyButton: true,
            confirmButtonText: `Yes`,
            denyButtonText: `No`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                $.ajax({
                    type: "POST",
                    url: "{{route('AjaxCallForDeleteProductImage')}}",
                    data: {
                        _token: "{{csrf_token()}}",
                        id: id
                    },
                    success: function (response) {
                        if(response[0])
                        {
                            $('#album').prop('disabled',false);
                            _this.closest('tr').remove();
                        }
                    }
                });
            } 
            else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    });

 
    // $(function () {
    //     // Summernote
    //     $('#summernote').summernote();
    //     $('#summernote1').summernote();
    //     $('#summernote2').summernote();
    // })
    
   

</script>
@endpush
