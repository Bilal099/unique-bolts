@extends('admin.layouts.app')

@push('custom-css')
<link rel="stylesheet" href="{{asset('backend/plugins/summernote/summernote-bs4.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.css"
    integrity="sha512-gX6K9e/4ewXjtn8Q/oePzgIxs2KPrksR4S2NNMYLxenvF7n7eNon9XbqQxb+5jcqYBVCcncIxqF6fXJYgQtoAg=="
    crossorigin="anonymous" />

@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <form id="quickForm" method="POST" action="{{route('home.store')}}">
                        @csrf
                        <div class="card card-primary mt-4">
                            <div class="card-header">
                                <h3 class="card-title">Home Page - Editor</h3>
                            </div>

                            

                            @if (Session::has('success'))
                            <div class="alert alert-info alert-dismissible m-3">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h5><i class="icon fas fa-info"></i> Success!</h5>
                                {{Session::get('success')}}
                            </div>
                            @elseif (Session::has('error'))
                            <div class="alert alert-danger alert-dismissible m-3">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h5><i class="icon fas fa-info"></i> Error!</h5>
                                {{Session::get('error')}}
                            </div>
                            @endif
                            <div class="card-body row">

                                {{-- <div class="form-group col-md-12">
                                    <h1 style="text-align: center">Title & Description</h1>
                                </div> --}}
                                <div class="form-group col-md-12">
                                    <label for="">Page Title <span style="color: red">*</span></label>
                                    <input type="text" name="title"
                                        class="form-control  @error('title') is-invalid @enderror"
                                        value="{{(old('title')!=null)? (old('title')):(isset($home->title)? $home->title:'')}}">
                                    @error('title')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                                
                                
                            </div>
                            <hr>
                            <div class="card-body row">
                                <h2>Meta Details</h2>
                                <div class="form-group col-md-12">
                                    <label for="">Meta Title <small>(Optional)</small> </label>
                                    <input type="text" name="meta_title"
                                        class="form-control @error('meta_title') is-invalid @enderror"
                                        value="{{(old('meta_title')!=null)? (old('meta_title')):(isset($home->meta_title)? $home->meta_title:'')}}">
                                    @error('meta_title')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="">Meta Keyword <small>(Optional)</small> </label>
                                    <input type="text" name="meta_keyword"
                                        class="form-control @error('meta_keyword') is-invalid @enderror"
                                        value="{{(old('meta_keyword')!=null)? (old('meta_keyword')):(isset($home->meta_keyword)? $home->meta_keyword:'')}}">
                                    @error('meta_keyword')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="">Meta Description <small>(Optional)</small> </label>
                                    <textarea name="meta_description" id="" cols="30" rows="5" class="form-control @error('meta_description') is-invalid @enderror">{{(old('meta_description')!=null)? (old('meta_description')):(isset($home->meta_description)? $home->meta_description:'')}}</textarea>
                                    {{-- <input type="text" name="description"
                                        class="form-control @error('description') is-invalid @enderror"
                                        value="{{(old('description')!=null)? (old('description')):(isset($home->description)? $home->description:'')}}"> --}}

                                    @error('meta_description')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>

                            <hr>

                            <div class="card-body row">
                                <h2>Main Heading </h2>
                                {{-- <div class="form-group col-md-12">
                                    <h2 style="text-align: center">Main Heading </h2>
                                </div> --}}
                                <div class="form-group col-md-12">
                                    <label for="">Main Title <span style="color: red">*</span></label>
                                    <input type="text" name="main_title"
                                        class="form-control  @error('main_title') is-invalid @enderror"
                                        value="{{(old('main_title')!=null)? (old('main_title')):(isset($content->main_title)? $content->main_title:'')}}">
                                    @error('main_title')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="">Main Description <span style="color: red">*</span></label>
                                    <input type="text" name="main_description"
                                        class="form-control @error('main_description') is-invalid @enderror"
                                        value="{{(old('main_description')!=null)? (old('main_description')):(isset($content->main_description)? $content->main_description:'')}}">
                                    @error('main_description')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                            <hr>
                            <div class="card-body row">

                                <h1> Why Choose Us?</h1>
                                <div class="form-group col-md-12">
                                    <label for="">Title <span style="color: red">*</span></label>
                                    <input type="text" name="why_choose_us_title"
                                        class="form-control  @error('why_choose_us_title') is-invalid @enderror"
                                        value="{{(old('why_choose_us_title')!=null)? (old('why_choose_us_title')):(isset($why_choose_us_detail->why_choose_us_title)? $why_choose_us_detail->why_choose_us_title:'')}}">
                                    @error('why_choose_us_title')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="">Description <span style="color: red">*</span></label>
                                    <input type="text" name="why_choose_us_description"
                                        class="form-control @error('why_choose_us_description') is-invalid @enderror"
                                        value="{{(old('why_choose_us_description')!=null)? (old('why_choose_us_description')):(isset($why_choose_us_detail->why_choose_us_description)? $why_choose_us_detail->why_choose_us_description:'')}}">
                                    @error('why_choose_us_description')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <h4 style="text-align: center">SECTION 1</h4>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="">Section Title <span style="color: red">*</span> </label>
                                    <input type="text" name="section1_title"
                                        class="form-control @error('section1_title') is-invalid @enderror"
                                        value="{{(old('section1_title')!=null)? (old('section1_title')):(isset($why_choose_us_detail->section1_title)? $why_choose_us_detail->section1_title:'')}}">
                                    @error('section1_title')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>



                                <div class="form-group col-md-12">
                                    <label for="">Section Description <span style="color: red">*</span> </label>
                                    <textarea id="summernote" name="section1_description"
                                        class="form-control @error('section1_description') is-invalid @enderror"
                                        cols="30"
                                        rows="10">{{(old('section1_description')!=null)? (old('section1_description')):(isset($why_choose_us_detail->section1_description)? $why_choose_us_detail->section1_description:'')}}</textarea>
                                    @error('section1_description')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>


                                <div class="form-group col-md-12">
                                    <h4 style="text-align: center">SECTION 2</h4>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="">Section Title <span style="color: red">*</span> </label>
                                    <input type="text" name="section2_title"
                                        class="form-control @error('section2_title') is-invalid @enderror"
                                        value="{{(old('section2_title')!=null)? (old('section2_title')):(isset($why_choose_us_detail->section2_title)? $why_choose_us_detail->section2_title:'')}}">
                                    @error('section2_title')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="">Section Description <span style="color: red">*</span> </label>
                                    <textarea id="summernote1" name="section2_description"
                                        class="form-control @error('section2_description') is-invalid @enderror" id=""
                                        cols="30"
                                        rows="10">{{(old('section2_description')!=null)? (old('section2_description')):(isset($why_choose_us_detail->section2_description)? $why_choose_us_detail->section2_description:'')}}</textarea>
                                    @error('section2_description')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>


                                <div class="form-group col-md-12">
                                    <h4 style="text-align: center">SECTION 3</h4>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="">Section Title <span style="color: red">*</span> </label>
                                    <input type="text" name="section3_title"
                                        class="form-control @error('section3_title') is-invalid @enderror"
                                        value="{{(old('section3_title')!=null)? (old('section3_title')):(isset($why_choose_us_detail->section3_title)? $why_choose_us_detail->section3_title:'')}}">
                                    @error('section3_title')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="">Section Description <span style="color: red">*</span> </label>
                                    <textarea id="summernote2" name="section3_description"
                                        class="form-control @error('section3_description') is-invalid @enderror" id=""
                                        cols="30"
                                        rows="10">{{(old('section3_description')!=null)? (old('section3_description')):(isset($why_choose_us_detail->section3_description)? $why_choose_us_detail->section3_description:'')}}</textarea>
                                    @error('section3_description')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>





                            </div>
                            <hr>
                            <div class="card-body row">

                                <h1> Last section on home screen  </h1>
                                <small>(Application link buttons and heading)</small>

                                <div class="form-group col-md-12">
                                    <label for="">Section Description <span style="color: red">*</span> </label>
                                    <textarea id="summernote3" name="last_section_description"
                                        class="form-control @error('last_section_description') is-invalid @enderror" 
                                        cols="30"
                                        rows="10">{{(old('last_section_description')!=null)? (old('last_section_description')):(isset($last_section->last_section_description)? $last_section->last_section_description:'')}}</textarea>
                                    @error('last_section_description')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="">Android Application Link <span style="color: red">*</span> </label>
                                    <input type="text" name="android_link"
                                        class="form-control  @error('android_link') is-invalid @enderror"
                                        value="{{(old('android_link')!=null)? (old('android_link')):(isset($last_section->android_link)? $last_section->android_link:'')}}">
                                    @error('android_link')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="">IOS Application Link <span style="color: red">*</span> </label>
                                    <input type="text" name="ios_link"
                                        class="form-control  @error('ios_link') is-invalid @enderror"
                                        value="{{(old('ios_link')!=null)? (old('ios_link')):(isset($last_section->ios_link)? $last_section->ios_link:'')}}">
                                    @error('ios_link')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" id="submit_btn">Submit</button>
                            </div>
                        </div>
                    </form>


                </div>
            </div>

        </div>
    </section>
</div>
@endsection

@push('custom-script')
<script src="{{asset('backend/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.js"
    integrity="sha512-+uGHdpCaEymD6EqvUR4H/PBuwqm3JTZmRh3gT0Lq52VGDAlywdXPBEiLiZUg6D1ViLonuNSUFdbL2tH9djAP8g=="
    crossorigin="anonymous"></script>

<script>
    $(function () {
        // Summernote
        $('#summernote').summernote();
        $('#summernote1').summernote();

        $('#summernote2').summernote();
        $('#summernote3').summernote();

    })

    $('#submit_btn').click(function (e) {
        e.preventDefault();

        Swal.fire({
            title: 'Do you want to save the changes?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: `Save`,
            denyButtonText: `Don't save`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                // Swal.fire('Saved!', '', 'success')
                $('#quickForm').submit();
            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })

</script>
@endpush
