@extends('admin.layouts.app')

@push('custom-css')
<link rel="stylesheet" href="{{asset('backend/plugins/summernote/summernote-bs4.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.css"
    integrity="sha512-gX6K9e/4ewXjtn8Q/oePzgIxs2KPrksR4S2NNMYLxenvF7n7eNon9XbqQxb+5jcqYBVCcncIxqF6fXJYgQtoAg=="
    crossorigin="anonymous" />

@endpush

@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary mt-4">
                        <div class="card-header">
                            <h3 class="card-title">About Us</h3>
                        </div>
                        @if (Session::has('success'))
                        <div class="alert alert-info alert-dismissible m-3">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> Success!</h5>
                            {{Session::get('success')}}
                        </div>
                        @elseif (Session::has('error'))
                        <div class="alert alert-danger alert-dismissible m-3">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> Error!</h5>
                            {{Session::get('error')}}
                        </div>
                        @endif
                        <form id="quickForm" method="POST" action="{{route('admin.aboutUs.store')}}" enctype="multipart/form-data">
                            @csrf

                            {{-- @if (isset($FrontPage->id))
                                <input type="hidden" name="id" >
                            @endif --}}
                            <div class="card-body row">
                                <div class="form-group col-md-12">
                                    <label for="">Page Title <span style="color: red">*</span></label>
                                    <input type="text" name="title"
                                        class="form-control  @error('title') is-invalid @enderror"
                                        value="{{(old('title')!=null)? (old('title')):(isset($FrontPage->title)? $FrontPage->title:'')}}">
                                    @error('title')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                                {{-- <div class="form-group col-md-12">
                                    <label for="">Page Description <small>(Optional)</small> </label>
                                    <input type="text" name="description"
                                        class="form-control @error('description') is-invalid @enderror"
                                        value="{{(old('description')!=null)? (old('description')):(isset($FrontPage->description)? $FrontPage->description:'')}}">
                                    @error('description')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div> --}}
                                
                            
                                <h2>Meta Details</h2>
                                <div class="form-group col-md-12">
                                    <label for="">Meta Title <small>(Optional)</small> </label>
                                    <input type="text" name="meta_title"
                                        class="form-control @error('meta_title') is-invalid @enderror"
                                        value="{{(old('meta_title')!=null)? (old('meta_title')):(isset($FrontPage->meta_title)? $FrontPage->meta_title:'')}}">
                                    @error('meta_title')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="">Meta Keyword <small>(Optional)</small> </label>
                                    <input type="text" name="meta_keyword"
                                        class="form-control @error('meta_keyword') is-invalid @enderror"
                                        value="{{(old('meta_keyword')!=null)? (old('meta_keyword')):(isset($FrontPage->meta_keywords)? $FrontPage->meta_keywords:'')}}">
                                    @error('meta_keyword')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="">Meta Description <small>(Optional)</small> </label>
                                    <textarea name="meta_description" id="" cols="30" rows="5" class="form-control @error('meta_description') is-invalid @enderror">{{(old('meta_description')!=null)? (old('meta_description')):(isset($FrontPage->meta_description)? $FrontPage->meta_description:'')}}</textarea>
                                    @error('meta_description')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                            
                                <div class="form-group col-md-12">
                                    <blockquote>
                                        <h1>Section 1</h1>
                                        <small>First section of About Us page</small>
                                    </blockquote>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="">Section Title <span style="color: red">*</span> </label>
                                    <input type="text" name="section1_title"
                                        class="form-control @error('section1_title') is-invalid @enderror"
                                        value="{{(old('section1_title')!=null)? (old('section1_title')):(isset($content->section1_title)? $content->section1_title:'')}}">
                                    @error('section1_title')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="">Section Image</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" accept="image/*" name="section1_image" class="custom-file-input @error('section1_image') is-invalid @enderror">
                                            <label class="custom-file-label" for="">Choose file</label>
                                        </div>
                                    </div>
                                    @error('section1_image')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                @if (isset($content->section1_image))
                                <div class="form-group col-md-6">
                                    <img src="{{ @asset('backend/images/aboutUs')."/".@$content->section1_image}}" height="150" width="auto" alt="" srcset="">
                                </div>
                                @endif

                                <div class="form-group col-md-12">
                                    <label for="">Section Description <span style="color: red">*</span> </label>
                                    <textarea id="summernote" name="section1_description"
                                        class="form-control @error('section1_description') is-invalid @enderror"
                                        cols="30"
                                        rows="10">{{(old('section1_description')!=null)? (old('section1_description')):(isset($content->section1_description)? $content->section1_description:'')}}</textarea>
                                    @error('section1_description')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>


                                <div class="form-group col-md-12">
                                    {{-- <h1 style="text-align: center">SECTION 1</h1> --}}
                                    <blockquote>
                                        <h1>Section 2</h1>
                                        <small>Second section of About Us page</small>
                                    </blockquote>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="">Section Title <span style="color: red">*</span> </label>
                                    <input type="text" name="section2_title"
                                        class="form-control @error('section2_title') is-invalid @enderror"
                                        value="{{(old('section2_title')!=null)? (old('section2_title')):(isset($content->section2_title)? $content->section2_title:'')}}">
                                    @error('section2_title')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="">Section Image</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" accept="image/*" name="section2_image" class="custom-file-input @error('section2_image') is-invalid @enderror">
                                            <label class="custom-file-label" for="">Choose file</label>
                                        </div>
                                    </div>
                                    @error('section2_image')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                @if (isset($content->section2_image))
                                <div class="form-group col-md-6">
                                    <img src="{{ @asset('backend/images/aboutUs')."/".@$content->section2_image}}" height="150" width="auto" alt="" srcset="">
                                </div>
                                @endif

                                <div class="form-group col-md-12">
                                    <label for="">Section Description <span style="color: red">*</span> </label>
                                    <textarea id="summernote1" name="section2_description"
                                        class="form-control @error('section2_description') is-invalid @enderror"
                                        cols="30"
                                        rows="10">{{(old('section2_description')!=null)? (old('section2_description')):(isset($content->section2_description)? $content->section2_description:'')}}</textarea>
                                    @error('section2_description')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>




                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" id="submit_btn">Submit</button>
                            </div>
                        </form>
                        <div class="card-body">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>
@endsection

@push('custom-script')
<script src="{{asset('backend/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.js"
    integrity="sha512-+uGHdpCaEymD6EqvUR4H/PBuwqm3JTZmRh3gT0Lq52VGDAlywdXPBEiLiZUg6D1ViLonuNSUFdbL2tH9djAP8g=="
    crossorigin="anonymous"></script>

<script>
    $(function () {
        // Summernote
        // $('#summernote').summernote();
        // $('#summernote1').summernote();
        // $('#summernote2').summernote();
    })

    $('#submit_btn').click(function (e) {
        e.preventDefault();

        Swal.fire({
            title: 'Do you want to save the changes?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: `Save`,
            denyButtonText: `Don't save`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                // Swal.fire('Saved!', '', 'success')
                $('#quickForm').submit();
            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })

</script>
@endpush
