@extends('admin.layouts.app')

@push('custom-css')
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.css"
    integrity="sha512-gX6K9e/4ewXjtn8Q/oePzgIxs2KPrksR4S2NNMYLxenvF7n7eNon9XbqQxb+5jcqYBVCcncIxqF6fXJYgQtoAg=="
    crossorigin="anonymous" />
<style>
    .text-limit {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        max-width: 150px;
    }

    .center {
        display: flex;
        justify-content: center;
        align-items: center;
    }

</style>
@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary mt-4">
                        <div class="card-header">
                            <h3 class="card-title">View Contact US</h3>
                        </div>

                        <div class="" style="display: none" id="msg_alert">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> <span id="ajax_msg_title">Success!</span> </h5>
                            <p id="ajax_msg"></p>
                        </div>
                        @if (Session::has('success'))
                        <div class="alert alert-info alert-dismissible m-3">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> Success!</h5>
                            {{Session::get('success')}}
                        </div>
                        @elseif (Session::has('error'))
                        <div class="alert alert-danger alert-dismissible m-3">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> Error!</h5>
                            {{Session::get('error')}}
                        </div>
                        @endif
                        <div class="card-body">
                            <button type="button" class="btn btn-primary float-right" data-toggle="modal"
                                data-target="#myModal">
                                Add Page Details
                            </button>
                            <table class="table table-bordered table-striped" id="data-table">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Message</th>
                                        <th>Action</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $key => $item)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td class="name">{{$item->name}}</td>
                                        <td class="email">{{$item->email}}</td>
                                        <td class="message text-limit">{{$item->message}}</td>
                                        <td class="center">
                                            <button class="btn btn-primary btn-sm view-btn" data-id="{{$item->id}}"><i
                                                    class="fa fa-eye"></i></button>
                                            {{-- <form action="{{route('ContactU.delete')}}" method="post">
                                                @csrf
                                                <input type="hidden" name="contactUs_id" value="{{$item->id}}">
                                                <button type="submit" class="btn btn-danger btn-sm ml-1 btn-delete">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </form> --}}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div>

        <!-- view Modal -->
        <div class="modal fade bd-example-modal-lg" id="view_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Contact Us Detail</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body" id="modal_body">
                        
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>

        <!-- The Modal -->
        <div class="modal fade" id="myModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form action="" method="post">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Contact US Page Details</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">

                                <div class="form-group col-md-12">
                                    <label for="">Page Title <span style="color: red">*</span></label>
                                    <input type="text" name="title" id="title"
                                        class="form-control  @error('title') is-invalid @enderror"
                                        value="{{(old('title')!=null)? (old('title')):(isset($FrontPage->title)? $FrontPage->title:'')}}">
                                    @error('title')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>


                            </div>
                            <div class="row">
                                <h2>Meta Details</h2>
                                <div class="form-group col-md-12">
                                    <label for="">Meta Title <small>(Optional)</small> </label>
                                    <input type="text" name="meta_title" id="meta_title"
                                        class="form-control @error('meta_title') is-invalid @enderror"
                                        value="{{(old('meta_title')!=null)? (old('meta_title')):(isset($FrontPage->meta_title)? $FrontPage->meta_title:'')}}">
                                    @error('meta_title')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="">Meta Keyword <small>(Optional)</small> </label>
                                    <input type="text" name="meta_keyword" id="meta_keyword"
                                        class="form-control @error('meta_keyword') is-invalid @enderror"
                                        value="{{(old('meta_keyword')!=null)? (old('meta_keyword')):(isset($FrontPage->meta_keywords)? $FrontPage->meta_keywords:'')}}">
                                    @error('meta_keyword')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="">Meta Description <small>(Optional)</small> </label>
                                    <textarea name="meta_description" id="meta_description" cols="30" rows="5"
                                        class="form-control @error('meta_description') is-invalid @enderror">{{(old('meta_description')!=null)? (old('meta_description')):(isset($FrontPage->meta_description)? $FrontPage->meta_description:'')}}</textarea>

                                    @error('meta_description')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button class="btn btn-primary" id="meta_detail_submit" type="submit">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </form>

                </div>

            </div>
        </div>

        
    </section>

</div>



@endsection


@push('custom-script')
<script src="{{asset('backend/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.js"
    integrity="sha512-+uGHdpCaEymD6EqvUR4H/PBuwqm3JTZmRh3gT0Lq52VGDAlywdXPBEiLiZUg6D1ViLonuNSUFdbL2tH9djAP8g=="
    crossorigin="anonymous"></script>

<script>
    $(function () {
        $("#data-table").DataTable();
    });

    $(document).ready(function () {

        $('.view-btn').click(function () {
            let _this = $(this);
            let id = _this.data('id');
            $.ajax({
                url: "{{route('AjaxCallForViewContactUs')}}",
                type: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                    id: id
                },

                success: function (result) {
                    console.log('result', result);
                    if (result) {
                        $('#view_modal').modal('show');
                        $('#modal_body').html(result);
                    }
                }

            })
        });
    });

    $('.btn-delete').click(function (e) {
        e.preventDefault();
        let _this = $(this);
        let form = _this.closest('form');
        Swal.fire({
            title: 'Are you sure, you want to Delete the selected record?',
            showDenyButton: true,
            // showCancelButton: true,
            confirmButtonText: `Yes`,
            denyButtonText: `No`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                // Swal.fire('Saved!', '', 'success')
                form.submit();
            } 
            else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })

    $("#meta_detail_submit").click(function (e) {
        e.preventDefault();

        let title = $("#title").val();
        let meta_title = $("#meta_title").val();
        let meta_keyword = $("#meta_keyword").val();
        let meta_description = $("#meta_description").val();

        if (title != '') {
            $.ajax({
                type: "POST",
                url: "{{route('AjaxCallForSaveContactUsMetaData')}}",
                data: {
                    _token: "{{ csrf_token() }}",
                    title: title,
                    meta_title: meta_title,
                    meta_keyword: meta_keyword,
                    meta_description: meta_description,

                },
                success: function (response) {
                    if (response['status']) {
                        $("#ajax_msg_title").text("Success");
                        $("#ajax_msg").text(response['msg']);
                        $("#myModal").modal('hide');
                        $("#msg_alert").addClass("alert alert-info alert-dismissible m-3");
                        $("#msg_alert").show();
                    } else {
                        $("#ajax_msg_title").text("Error");
                        $("#ajax_msg").text(response['msg']);
                        $("#myModal").modal('hide');
                        $("#msg_alert").addClass("alert alert-danger alert-dismissible m-3");

                        $("#msg_alert").show();
                    }
                }
            });
        } else {
            alert('Title field is empty.');
            $("#title").focus();
            // $("#myModal").modal('hide');
        }

    });

</script>
@endpush
