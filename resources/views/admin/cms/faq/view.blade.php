@extends('admin.layouts.app')
@push('custom-css')
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.css"
    integrity="sha512-gX6K9e/4ewXjtn8Q/oePzgIxs2KPrksR4S2NNMYLxenvF7n7eNon9XbqQxb+5jcqYBVCcncIxqF6fXJYgQtoAg=="
    crossorigin="anonymous" />
@endpush
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary mt-4">
                        <div class="card-header">
                            <h3 class="card-title">View {{ @$title}}</h3>
                        </div>
                        <div class="" style="display: none" id="msg_alert">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> <span id="ajax_msg_title">Success!</span> </h5>
                            <p id="ajax_msg"></p>
                        </div>
                        @if (Session::has('success'))
                        <div class="alert alert-info alert-dismissible m-3">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> Success!</h5>
                            {{Session::get('success')}}
                        </div>
                        @elseif (Session::has('error'))
                        <div class="alert alert-danger alert-dismissible m-3">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> Error!</h5>
                            {{Session::get('error')}}
                        </div>
                        @endif

                        <!-- /.card-header -->
                        <div class="card-body">
                            <a href="{{route('faq.add')}}" class="btn btn-primary mb-3">Add {{ @$title}}</a>
                            <button type="button" class="btn btn-primary float-right" data-toggle="modal"
                                data-target="#myModal">
                                Add Page Details
                            </button>
                            <table class="table table-bordered table-striped" id="data-table">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Title</th>
                                        <th>Active</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $key => $item)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->active == 1?'active':'deactive'}}</td>
                                        <td>
                                            <a href="{{route('faq.add')}}/{{$item['id']}}"
                                                class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                            {{-- <a href="{{route('faq.delete', $item['id'])}}" rel="delete"
                                            class="ajax btn-danger btn btn-sm" data-id="$item['id']"><i
                                                class="fa fa-times"></i></a> --}}
                                            <button rel="delete" class="ajax btn-danger btn btn-sm delete-btn"
                                                data-id="{{$item['id']}}"><i class="fa fa-times"></i></button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
        <!-- The Modal -->
        <div class="modal fade" id="myModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form action="" method="post">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">FAQ Page Details</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">

                                <div class="form-group col-md-12">
                                    <label for="">Page Title <span style="color: red">*</span></label>
                                    <input type="text" name="title" id="title"
                                        class="form-control  @error('title') is-invalid @enderror"
                                        value="{{(old('title')!=null)? (old('title')):(isset($FrontPage->title)? $FrontPage->title:'')}}">
                                    @error('title')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>


                            </div>
                            <div class="row">
                                <h2>Meta Details</h2>
                                <div class="form-group col-md-12">
                                    <label for="">Meta Title <small>(Optional)</small> </label>
                                    <input type="text" name="meta_title" id="meta_title"
                                        class="form-control @error('meta_title') is-invalid @enderror"
                                        value="{{(old('meta_title')!=null)? (old('meta_title')):(isset($FrontPage->meta_title)? $FrontPage->meta_title:'')}}">
                                    @error('meta_title')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="">Meta Keyword <small>(Optional)</small> </label>
                                    <input type="text" name="meta_keyword" id="meta_keyword"
                                        class="form-control @error('meta_keyword') is-invalid @enderror"
                                        value="{{(old('meta_keyword')!=null)? (old('meta_keyword')):(isset($FrontPage->meta_keyword)? $FrontPage->meta_keyword:'')}}">
                                    @error('meta_keyword')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="">Meta Description <small>(Optional)</small> </label>
                                    <textarea name="meta_description" id="meta_description" cols="30" rows="5"
                                        class="form-control @error('meta_description') is-invalid @enderror">{{(old('meta_description')!=null)? (old('meta_description')):(isset($FrontPage->meta_description)? $FrontPage->meta_description:'')}}</textarea>

                                    @error('meta_description')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button class="btn btn-primary" id="meta_detail_submit" type="submit">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </form>

                </div>

            </div>
        </div>
    </section>
    <!-- /.content -->
</div>



@endsection
@push('custom-script')
<script src="{{asset('backend/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.js"
    integrity="sha512-+uGHdpCaEymD6EqvUR4H/PBuwqm3JTZmRh3gT0Lq52VGDAlywdXPBEiLiZUg6D1ViLonuNSUFdbL2tH9djAP8g=="
    crossorigin="anonymous"></script>
<script>
    $(function () {
        $("#data-table").DataTable();
    });


    $('.delete-btn').click(function (e) {
        e.preventDefault();
        let id = $(this).data('id');
        Swal.fire({
            title: 'Are you sure, you want to Delete the selected record?',
            showDenyButton: true,
            // showCancelButton: true,
            confirmButtonText: `Yes`,
            denyButtonText: `No`,
        }).then((result) => {
            if (result.isConfirmed) {
                // $('#quickForm').submit();
                window.location.href = '/admin/faq/delete/' + id;

            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })

    $("#meta_detail_submit").click(function (e) {
        e.preventDefault();

        let title = $("#title").val();
        let meta_title = $("#meta_title").val();
        let meta_keyword = $("#meta_keyword").val();
        let meta_description = $("#meta_description").val();

        if (title != '') {
            $.ajax({
                type: "POST",
                url: "{{route('AjaxCallForSaveFaqMetaData')}}",
                data: {
                    _token: "{{ csrf_token() }}",
                    title: title,
                    meta_title: meta_title,
                    meta_keyword: meta_keyword,
                    meta_description: meta_description,

                },
                success: function (response) {
                    if (response['status']) {
                        $("#ajax_msg_title").text("Success");
                        $("#ajax_msg").text(response['msg']);
                        $("#myModal").modal('hide');
                        $("#msg_alert").addClass("alert alert-info alert-dismissible m-3");
                        $("#msg_alert").show();
                    } else {
                        $("#ajax_msg_title").text("Error");
                        $("#ajax_msg").text(response['msg']);
                        $("#myModal").modal('hide');
                        $("#msg_alert").addClass("alert alert-danger alert-dismissible m-3");

                        $("#msg_alert").show();
                    }
                }
            });
        } else {
            alert('Title field is empty.');
            $("#title").focus();
            // $("#myModal").modal('hide');
        }

    });


    // window.location.href = "http://stackoverflow.com";

</script>
@endpush
