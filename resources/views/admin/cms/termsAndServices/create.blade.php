@extends('admin.layouts.app')

@push('custom-css')
<link rel="stylesheet" href="{{asset('backend/plugins/summernote/summernote-bs4.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.css"
    integrity="sha512-gX6K9e/4ewXjtn8Q/oePzgIxs2KPrksR4S2NNMYLxenvF7n7eNon9XbqQxb+5jcqYBVCcncIxqF6fXJYgQtoAg=="
    crossorigin="anonymous" />

@endpush

@section('content')

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <form id="quickForm" method="POST" action="{{route('termsAndServices.store')}}">
                        @csrf
                        <div class="card card-primary mt-4">
                            <div class="card-header">
                                <h3 class="card-title">Term And Services Page - Editor</h3>
                            </div>

                            @if (Session::has('success'))
                            <div class="alert alert-info alert-dismissible m-3">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h5><i class="icon fas fa-info"></i> Success!</h5>
                                {{Session::get('success')}}
                            </div>
                            @elseif (Session::has('error'))
                            <div class="alert alert-danger alert-dismissible m-3">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h5><i class="icon fas fa-info"></i> Error!</h5>
                                {{Session::get('error')}}
                            </div>
                            @endif
                            <div class="card-body row">


                                <div class="form-group col-md-12">
                                    <label for="">Page Title <span style="color: red">*</span></label>
                                    <input type="text" name="title"
                                        class="form-control  @error('title') is-invalid @enderror"
                                        value="{{(old('title')!=null)? (old('title')):(isset($termsAndServices->title)? $termsAndServices->title:'')}}">
                                    @error('title')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                                {{-- <div class="form-group col-md-12">
                                    <label for="">Page Description <small>(Optional)</small> </label>
                                    <input type="text" name="description"
                                        class="form-control @error('description') is-invalid @enderror"
                                        value="{{(old('description')!=null)? (old('description')):(isset($termsAndServices->description)? $termsAndServices->description:'')}}">
                                    @error('description')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div> --}}
                            </div>
                            <hr>
                            <div class="card-body row">
                                <h2>Meta Details</h2>
                                <div class="form-group col-md-12">
                                    <label for="">Meta Title <small>(Optional)</small> </label>
                                    <input type="text" name="meta_title"
                                        class="form-control @error('meta_title') is-invalid @enderror"
                                        value="{{(old('meta_title')!=null)? (old('meta_title')):(isset($termsAndServices->meta_title)? $termsAndServices->meta_title:'')}}">
                                    @error('meta_title')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="">Meta Keyword <small>(Optional)</small> </label>
                                    <input type="text" name="meta_keyword"
                                        class="form-control @error('meta_keyword') is-invalid @enderror"
                                        value="{{(old('meta_keyword')!=null)? (old('meta_keyword')):(isset($termsAndServices->meta_keyword)? $termsAndServices->meta_keyword:'')}}">
                                    @error('meta_keyword')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="">Meta Description <small>(Optional)</small> </label>
                                    <textarea name="meta_description" id="" cols="30" rows="5" class="form-control @error('meta_description') is-invalid @enderror">{{(old('meta_description')!=null)? (old('meta_description')):(isset($termsAndServices->meta_description)? $termsAndServices->meta_description:'')}}</textarea>

                                    @error('meta_description')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                            <hr>

                            <div class="card-body row">
                                <h2>Main Heading </h2>
                                <div class="form-group col-md-12">
                                    <label for="">Main Title <span style="color: red">*</span></label>
                                    <input type="text" name="main_title"
                                        class="form-control  @error('main_title') is-invalid @enderror"
                                        value="{{(old('main_title')!=null)? (old('main_title')):(isset($content->main_title)? $content->main_title:'')}}">
                                    @error('main_title')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="">Main Description <span style="color: red">*</span></label>
                                    <textarea id="summernote" name="main_description"
                                        class="form-control @error('main_description') is-invalid @enderror" cols="30"
                                        rows="10">{{(old('main_description')!=null)? (old('main_description')):(isset($content->main_description)? $content->main_description:'')}}</textarea>
                                    @error('main_description')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" id="submit_btn">Submit</button>
                            </div>
                        </div>
                    </form>


                </div>
            </div>

        </div>
    </section>
</div>
@endsection

@push('custom-script')
<script src="{{asset('backend/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.js"
    integrity="sha512-+uGHdpCaEymD6EqvUR4H/PBuwqm3JTZmRh3gT0Lq52VGDAlywdXPBEiLiZUg6D1ViLonuNSUFdbL2tH9djAP8g=="
    crossorigin="anonymous"></script>

<script>
    $(function () {
        // Summernote
        $('#summernote').summernote();
    })

    $('#submit_btn').click(function (e) {
        e.preventDefault();

        Swal.fire({
            title: 'Do you want to save the changes?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: `Save`,
            denyButtonText: `Don't save`,
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                // Swal.fire('Saved!', '', 'success')
                $('#quickForm').submit();
            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })

</script>
@endpush
