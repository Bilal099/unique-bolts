 <!-- Main Sidebar Container -->
 <aside class="main-sidebar sidebar-dark-primary elevation-4">
     <!-- Brand Logo -->
     <a href="index3.html" class="brand-link">
         <img src="{{asset('backend/dist/img/icon.png')}}" alt="AdminLTE Logo" class="brand-image " style="opacity: .8">
         <span class="brand-text font-weight-light">Unique Bolts</span>
     </a>

     <!-- Sidebar -->
     <div class="sidebar">
         <!-- Sidebar user panel (optional) -->
         <div class="user-panel mt-3 pb-3 mb-3 d-flex">
             <div class="image">
                 <img src="{{asset('backend/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2"
                     alt="User Image">
             </div>
             <div class="info">
                 <a href="#" class="d-block">{{ @Auth::user()->name }}</a>
             </div>
         </div>

         <!-- SidebarSearch Form -->
         {{-- <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div> --}}

         <!-- Sidebar Menu -->
         <nav class="mt-2">
             <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                 data-accordion="false">
                 <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

                 <li class="nav-item">
                     <a href="{{route('admin.category')}}" class="nav-link {{ ( Route::currentRouteName() == 'admin.category' || Route::currentRouteName() == 'admin.categoryCreate' || Route::currentRouteName() == 'admin.categoryEdit')? 'active' : '' }}">
                         <i class="fas fa-th nav-icon"></i>
                         <p>
                             Category
                         </p>
                     </a>
                 </li>
                 <li class="nav-item">
                    <a href="{{route('admin.product')}}" class="nav-link {{ ( Route::currentRouteName() == 'admin.product' || Route::currentRouteName() == 'admin.productCreate' || Route::currentRouteName() == 'admin.productEdit')? 'active' : '' }}">
                        <i class="fas fa-boxes nav-icon"></i>
                        <p>
                            Product
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('admin.partner')}}" class="nav-link {{ ( Route::currentRouteName() == 'admin.partner' || Route::currentRouteName() == 'admin.partnerCreate' || Route::currentRouteName() == 'admin.partnerEdit')? 'active' : '' }}">
                        <i class="fas fa-handshake nav-icon"></i>
                        <p>
                            Partners
                        </p>
                    </a>
                </li>
                    @php
                        $arr = array("admin.ContactUs", "admin.InquiryForm", "admin.aboutUs");
                    @endphp
                 <li class="nav-item {{ in_array(Route::currentRouteName(),$arr)? 'menu-is-opening menu-open' : '' }}">
                     <a href="#" class="nav-link">
                         <i class="nav-icon fas fa-chart-pie"></i>
                         <p>
                             CMS
                             <i class="right fas fa-angle-left"></i>
                         </p>
                     </a>
                     <ul class="nav nav-treeview">
                         <li class="nav-item">
                             <a href="{{route('admin.aboutUs')}}" class="nav-link {{ ( Route::currentRouteName() == 'admin.aboutUs' ) ? 'active' : '' }}">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>
                                     About Us
                                 </p>
                             </a>
                         </li>
                         <li class="nav-item">
                             <a href="{{route('admin.InquiryForm')}}" class="nav-link {{ ( Route::currentRouteName() == 'admin.InquiryForm' ) ? 'active' : '' }}">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>
                                     Inquiry Form
                                 </p>
                             </a>
                         </li>

                         <li class="nav-item">
                             <a href="{{route('admin.ContactUs')}}" class="nav-link {{ ( Route::currentRouteName() == 'admin.ContactUs' ) ? 'active' : '' }}">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>
                                     Contact Us
                                 </p>
                             </a>
                         </li>
                     </ul>
                 </li>

                 {{-- <li class="nav-item">
                     <a href="#" class="nav-link">
                         <i class="nav-icon fas fa-chart-pie"></i>
                         <p>
                             Charts
                             <i class="right fas fa-angle-left"></i>
                         </p>
                     </a>
                     <ul class="nav nav-treeview">
                         <li class="nav-item">
                             <a href="pages/charts/chartjs.html" class="nav-link">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>ChartJS</p>
                             </a>
                         </li>
                         <li class="nav-item">
                             <a href="pages/charts/flot.html" class="nav-link">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>Flot</p>
                             </a>
                         </li>
                         <li class="nav-item">
                             <a href="pages/charts/inline.html" class="nav-link">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>Inline</p>
                             </a>
                         </li>
                         <li class="nav-item">
                             <a href="pages/charts/uplot.html" class="nav-link">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>uPlot</p>
                             </a>
                         </li>
                     </ul>
                 </li>
                 <li class="nav-item">
                     <a href="#" class="nav-link">
                         <i class="nav-icon fas fa-tree"></i>
                         <p>
                             UI Elements
                             <i class="fas fa-angle-left right"></i>
                         </p>
                     </a>
                     <ul class="nav nav-treeview">
                         <li class="nav-item">
                             <a href="pages/UI/general.html" class="nav-link">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>General</p>
                             </a>
                         </li>
                         <li class="nav-item">
                             <a href="pages/UI/icons.html" class="nav-link">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>Icons</p>
                             </a>
                         </li>
                     </ul>
                 </li> --}}
             </ul>
         </nav>
         <!-- /.sidebar-menu -->
     </div>
     <!-- /.sidebar -->
 </aside>
