@extends('website.layouts.app')

@push('custom-css')
    <style>
        @media (max-width: 1331px) {
            ol.carousel-indicators{
                display: none !important;
            }
        }
    </style>
@endpush

@section('content')

<div id="carouselExampleIndicators" class="carousel slide section-1" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="slider-social">
        <a href="#"><i class="fab fa-whatsapp" aria-hidden="true"></i></a>
        <a href="#"><i class="fab fa-facebook" aria-hidden="true"></i></a>
        <a href="#"><i class="fab fa-instagram" aria-hidden="true"></i></a>
        <a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a>
    </div>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src="{{ asset('/website/images/slider-data1.png') }}" alt="First slide">
            <div class="carousel-caption d-md-block">
                <h5><span>NUTS, BOLTS</span><br>
                    AND FASTENERS
                </h5>
                <p>Construction, Manufacturing,</p>
                <p>AG Equipment, Machinery, </p>
                <p>Automotive, Electrical, Plumbing</p>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="{{ asset('/website/images/slider-data1.png') }}" alt="Second slide">
            <div class="carousel-caption d-md-block">

                <h5><span>NUTS, BOLTS</span><br>
                    AND FASTENERS
                </h5>
                <p>Construction, Manufacturing,</p>
                <p>AG Equipment, Machinery, </p>
                <p>Automotive, Electrical, Plumbing</p>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="{{ asset('/website/images/slider-data1.png') }}" alt="Third slide">
            <div class="carousel-caption d-md-block">
                <h5><span>NUTS, BOLTS</span><br>
                    AND FASTENERS
                </h5>
                <p>Construction, Manufacturing,</p>
                <p>AG Equipment, Machinery, </p>
                <p>Automotive, Electrical, Plumbing</p>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>




<div class="container">

</div>

@if (count($is_featured) > 0)
<div class="container-fluid py-5">
    <span class="text-left text-uppercase div-head">FEATURED CATEGORIES&nbsp;&nbsp;</span>
    <div id="iycnj" class="container pt-1 featured-cat-border">
        <div class="row">
            {{-- <div class="card py-3"><img src="{{asset('website/images')}}/shop-featured.png" alt="image alt"
            class="img-fluid-home " />
            <span class=" display-1 text-center text-uppercase">CARRIAGE BOLTS</span>
        </div>
        <div class="card py-3"><img src="{{asset('website/images')}}/shop-featured.png" alt="image alt"
                class="img-fluid-home" />
            <span class=" display-1 text-center text-uppercase">CARRIAGE BOLTS</span>
        </div>
        <div class="card py-3"><img src="{{asset('website/images')}}/shop-featured.png" alt="image alt"
                class="img-fluid-home" />
            <span class=" display-1 text-center text-uppercase">SQUARE HEAD BOLTS</span>
        </div>
        <div class="card py-3"><img src="{{asset('website/images')}}/shop-featured.png" alt="image alt"
                class="img-fluid-home" />
            <span class=" display-1 text-center text-uppercase">FLANGE HEAD HEX BOLT</span>
        </div>
        <div class="card py-3"><img src="{{asset('website/images')}}/shop-featured.png" alt="image alt"
                class="img-fluid-home" />
            <span class=" display-1 text-center text-uppercase">EYE BOLTS</span>
        </div> --}}

        @foreach ($category as $item)
        @if ($item->featured_category == 1)
        <div class="card py-3">
			<a href="{{route('website.subCategory',$item->id)}}">
			<img src="{{asset('backend/images/category').'/'.((@$item->image!="")? @$item->image:"dummy_image.png")}}" alt="image alt"
                class="img-fluid-home " />
            <span class=" display-home text-center text-uppercase">{{@$item->name}}</span>
		</a>
        </div>
        @endif
        @endforeach

    </div>
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
            <a class="xpand-cat" href="{{ route('website.shop') }}">
                <span class="float-right">&nbsp;EXPAND CATEGORY</span>
                <span class="arrow-badge float-right"><i class="fas fa-long-arrow-alt-right"></i></span>
            </a>
        </div>
    </div>
</div>
</div>
@endif

<div class="container-fluid py-3">
    <span class="text-left text-uppercase div-head">CATALOG CATEGORIES&nbsp;&nbsp;</span>
    <div class="container pt-1">
        <div class="row blue-cls">
    {{-- <div class="card py-3"><img src="{{asset('website/images')}}/shop-featured.png" alt="image alt"
            class="img-fluid-home" />
            <span class=" display-1 text-center text-uppercase ">CARRIAGE BOLTS</span>
        </div>
        <div class="card py-3"><img src="{{asset('website/images')}}/shop-featured.png" alt="image alt"
                class="img-fluid-home" />
            <span class=" display-1 text-center text-uppercase ">CARRIAGE BOLTS</span>
        </div>
        <div class="card py-3"><img src="{{asset('website/images')}}/shop-featured.png" alt="image alt"
                class="img-fluid-home" />
            <span class=" display-1 text-center text-uppercase ">SQUARE HEAD BOLTS</span>
        </div>
        <div class="card py-3"><img src="{{asset('website/images')}}/shop-featured.png" alt="image alt"
                class="img-fluid-home" />
            <span class=" display-1 text-center text-uppercase ">FLANGE HEADHEX BOLT</span>
        </div>
        <div class="card py-3"><img src="{{asset('website/images')}}/shop-featured.png" alt="image alt"
                class="img-fluid-home" />
            <span class=" display-1 text-center text-uppercase ">EYE BOLTS</span>
        </div>
        <div class="clearfix"></div>
        <div class="card py-3 img-blur"><img src="{{asset('website/images')}}/shop-featured.png" alt="image alt"
                class="img-fluid-home" />
            <span class=" display-1 text-center text-uppercase ">CARRIAGE BOLTS</span>
        </div>
        <div class="card py-3 img-blur"><img src="{{asset('website/images')}}/shop-featured.png" alt="image alt"
                class="img-fluid-home" />
            <span class=" display-1 text-center text-uppercase ">CARRIAGE BOLTS</span>
        </div>
        <div class="card py-3 img-blur"><img src="{{asset('website/images')}}/shop-featured.png" alt="image alt"
                class="img-fluid-home" />
            <span class=" display-1 text-center text-uppercase ">SQUARE HEAD BOLTS</span>
        </div>
        <div class="card py-3 img-blur"><img src="{{asset('website/images')}}/shop-featured.png" alt="image alt"
                class="img-fluid-home" />
            <span class=" display-1 text-center text-uppercase ">FLANGE HEADHEX BOLT</span>
        </div>
        <div class="card py-3 img-blur"><img src="{{asset('website/images')}}/shop-featured.png" alt="image alt"
                class="img-fluid-home" />
            <span class=" display-1 text-center text-uppercase ">EYE BOLTS</span>
        </div> 
	--}}

        @php
        $i = 0;
        @endphp
        @foreach ($category as $item)
        @if ($item->featured_category == 0 && $item->parent_id == null)
        <div class="card py-3 blogBox moreBox " style="display: none !important;">
			<a href="{{route('website.subCategory',$item->id)}}">
			<img src="{{asset('backend/images/category').'/'.((@$item->image!="")? @$item->image:"dummy_image.png")}}" alt="image alt" class="img-fluid-home " />
            <span class=" display-home text-center text-uppercase">{{@$item->name}}</span>
			</a>
        </div>
        @endif
        @endforeach

    </div>
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
            <p class="text-center loadmore moreBox" id="loadMore">LOAD MORE +</p>
        </div>
    </div>
</div>
<div class="row row-cookies">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
        <p id="iovpf" class="pb-2 mb-0">UNIQUEBOLTS USES COOKIES TO IMPROVE USER EXPERIENCE</p>
    </div>
    <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7 col-12">
        <p id="i1lbh">LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISCING ELIT, SED DO EIUSMOD TEMPOR INCIDIDUNT UT
            LABORE ET DOLORE MAGNA ALIQUA. UT ENIM AD MINIM VENIAM, QUIS NOSTRUD
            EXERCITATION ULLAMCO LABORIS NISI UT ALIQUIP EX EA COMMODO CONSEQUAT. DUIS AUTE IRURE DOLOR IN REPREHENDERIT
            IN VOLUPTATE VELIT ESSE CILLUM DOLORE EU FUGIAT NULLA PARIATUR.</p>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12 accept-cookie-btn">
        <a href="#" class="btn btn-block accept_continue-btn">ACCEPT & CONTINUE</a>
    </div>
    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-12 text-center block-cookie-text">
        <span id="irpxl" class="text-center">I WISH TO BLOCK COOKIES</span>
    </div>
</div>
</div>

@endsection

@push("custom-script")
<script>
    $(document).ready(function () {
        var windowSize = $(window).width();
        var slice = 0;
        var blur_slice =0;
        if (windowSize <= 2560 && windowSize > 1690) 
        {
          slice =10;
          blur_slice = 5;
        }
        else if (windowSize <= 1690 && windowSize > 1280) 
        {
          slice =8;
          blur_slice = 4;
        }
        else if (windowSize <= 1280 && windowSize > 1024) 
        {
          slice =6;
          blur_slice = 3;
        }
        else if (windowSize <= 1024 && windowSize > 768) 
        {
          slice =4;
          blur_slice = 2;
        }
        else if (windowSize <= 768) 
        {
          slice =2;
          blur_slice = 1;
        }
        $(".moreBox").slice(0, slice).show();
        $(".moreBox").slice(blur_slice, slice).addClass('img-blur');

        if ($(".blogBox:hidden").length != 0) {
            $("#loadMore").show();
        }
        
        $("#loadMore").on("click", function (e) {
          var load_slice = 0;
            e.preventDefault(); 
            if (windowSize <= 2560 && windowSize > 1690){
              load_slice = 5;
            }
            else if (windowSize <= 1690 && windowSize > 1280) {
              load_slice = 4;
            }
            else if (windowSize <= 1280  && windowSize > 1024) {
              load_slice = 3;
            }
            else if (windowSize <= 1024  && windowSize > 768) {
              load_slice = 2;
            }
            else if (windowSize <= 768) {
              load_slice = 1;
            }
            $(".moreBox:visible").removeClass('img-blur');
              $(".moreBox:hidden").slice(0, load_slice).addClass('img-blur');
              $(".moreBox:hidden").slice(0, load_slice).slideDown();
              if ($(".moreBox:hidden").length == 0) {
                  $("#loadMore").fadeOut("slow");
                  $(".moreBox:visible").removeClass('img-blur');
              }
        });
    });

</script>
@endpush
