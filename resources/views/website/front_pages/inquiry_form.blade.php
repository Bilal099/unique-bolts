<!DOCTYPE html>
<html lang="en">
<head>
	<title>Inquiry Form</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
     <!-- Google Font: Source Sans Pro -->
     <link rel="stylesheet"
     href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
 <!-- Font Awesome -->
 <link rel="stylesheet" href="{{asset('backend/plugins/fontawesome-free/css/all.min.css')}}">
 <!-- icheck bootstrap -->
 <link rel="stylesheet" href="{{asset('backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
 <!-- Theme style -->
 <link rel="stylesheet" href="{{asset('backend/dist/css/adminlte.min.css')}}">
</head>
<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="{{route('login')}}"><b>Template For Website</b></a>
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Inquiry Form</p>

                <form id="contact_us_form">
                    {{-- <span class="contact100-form-title">
                        Send Us A Message
                    </span> --}}

                    <div class="mb-2">
                        <label for="">Full Name</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Full Name">
                        
                    </div>

                    <div class="mb-2">
                        <label for="">Email</label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                        
                    </div>

                    <div class="mb-2">
                        <label for="">Subject</label>
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject">
                        
                    </div>

                    <div class=" mb-2">
                        <label for="">Message</label>
                        <textarea type="text" class="form-control" name="message" id="message" placeholder="Message"> </textarea>
                        <div class="input-group-append">
                            
                        </div>
                    </div>
    
                  
    

                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block btn-contactUs">Send</button>
                    </div>
                </form>

                
            </div>
        </div>
    </div>




<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<!-- jQuery -->
<script src="{{asset('backend/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('backend/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('backend/dist/js/adminlte.min.js')}}"></script>

<script>
    $(".btn-contactUs").click(function (e) { 
        e.preventDefault();

        let name    = $("#name").val();
        let email   = $("#email").val();
        let subject = $("#subject").val();
        let message = $("#message").val();

        if(name == "" || email == "" || subject == "" || message == "" )
        {
            alert('please fill all fields')
        }
        else{
            $.ajax({
            type: "POST",
            url: "{{route('AjaxCallForInquiryForm')}}",
            data: {
                _token: "{{ csrf_token() }}",
                name: name,
                email: email,
                subject: subject,
                message: message,
            },
            success: function (response) {
                alert(response['message']);
                $("#name").val("");
                $("#email").val("");
                $("#subject").val("");
                $("#message").val("");
            }
        });
        }

    });
</script>


</body>
</html>
