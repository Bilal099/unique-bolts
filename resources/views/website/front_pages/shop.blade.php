@extends('website.layouts.app')

@section('content')

<div id="iko5" class="hero-image-shop">
    <div class="hero-text">
        <h1 id="ik9h">SHOP</h1>
    </div>
</div>
<div class="shop-carousel">
    <div class="container">
        <h2 class="s-latest-product">Latest Products</h2>
        <div class="owl-carousel reponsive owl-theme">
            @php
            $j = 1;
            @endphp
            @foreach ($product as $item)
            <div class="item">
                <a href="{{route('website.productDetails',$item->id)}}">
                <img src="{{asset('backend/images/product').'/'.((@$item->image!="")? @$item->image:"dummy_image.png")}}" />
                <h3 class="shop-carousel-title">{{@$item->name}}</h3></a>
            </div>
            @if ($j++ == count($product))
            @break
            @endif

            @endforeach


        </div>


    </div>
</div>
<div class="container-fluid py-3 shop-sec-prod">

    <div class="container pt-1 custom-pad">
        <h2 class="s-latest-product text-center">CATEGORIES</h2>
        @php
        $i = 0;
        @endphp
        <div class="row">
            @foreach ($category as $item)

            @if (@$item->parent_id == null)
            <div class="card py-3">
                <a href="{{route('website.subCategory',$item->id)}}" class="shop-p-link"><img
                        src="{{asset('backend/images/category').'/'.((@$item->image!="")? @$item->image:"dummy_image.png")}}" alt="image alt"
                        class="product-dynamic-images">
                    <span class=" display-1 text-center text-uppercase shop-carousel-title">{{@$item->name}}</span></a>
            </div>
            @endif

            @php
            $i++;
            @endphp
            @endforeach
        </div>
    </div>
</div>





@endsection

@push("custom-css")
<link rel="stylesheet" href="{{asset('backend/plugins/owlcarousel/dist/assets/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/owlcarousel/dist/assets/owl.theme.default.min.css')}}">
<style>

</style>
@endpush
@push("custom-script")

<script>
    $(document).ready(function () {

        $(".owl-carousel").owlCarousel({
            loop: false,
            margin: 15,
            nav: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                },
                1200: {
                    items: 5
                }
            }
        });

    });

</script>

<script src="{{asset('backend/plugins/owlcarousel/dist/owl.carousel.min.js')}}"></script>
@endpush
