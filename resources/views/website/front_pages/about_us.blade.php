@extends('website.layouts.app')

@section('content')

<div id="iko5" class="hero-image-about">
    <div class="hero-text">
        <h1 id="ik9h">ABOUT US!</h1>
    </div>
</div>
<div class="container-fluid">
    <div class="row mi-row1">
        <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-xs-12 ">
            {{-- <img src="{{asset('website/images')}}/About-us-img.png" class="about_pg-img"> --}}
            <img src="{{ @asset('backend/images/aboutUs')."/".@$content->section1_image}}" class="about_pg-img">
        </div>
        <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12">
            <h2 class="about_pg-headng">{{ @$content->section1_title }}</h2>
            {{-- <p class="about_pg-para">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis
                nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem
                vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu
                feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum
                zzril delenit augue duis dolore te feugait nulla facilisi.</p>
            <p class="about_pg-para">Abbas Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh
                euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis
                nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p> --}}
                <p class="about_pg-para">
                    {!! @$content->section1_description !!}
                </p>
        </div>
    </div>

    <div class="row mi-row2">
        <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12">
            {{-- <h2 class="about_pg-headng2">FULL NAME</h2>
            <p class="about_pg-para2">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis
                nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem
                vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu
                feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum
                zzril delenit augue duis dolore te feugait nulla facilisi.</p>
            <p class="about_pg-para2">Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh
                euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis
                nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p> --}}

                <h2 class="about_pg-headng2">{{ @$content->section2_title }}</h2>
                <p class="about_pg-para">{!! @$content->section2_description !!}</p>


        </div>
        <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-xs-12 ">
            {{-- <img src="{{asset('website/images')}}/About-us-img.png" class="about_pg-img"> --}}
            <img src="{{ @asset('backend/images/aboutUs')."/".@$content->section2_image}}" class="about_pg-img">

        </div>
    </div>

    <div>
        <h1 class="getHeading">GET IN TOUCH</h1>
        <form action="" class="contact-form">
            <div class="Inputrow row cf-row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4  mt-2 mb-4">
                    <input type="text" name="name" id="name" class="form-control" placeholder="Name*">
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4  mt-2 mb-4">
                    <input type="text" name="email" id="email" class="form-control" placeholder="Email*">
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-xs-4  mt-2 mb-4">
                    <input type="text" name="subject" id="subject" class="form-control" placeholder="Subject*">
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-2 mb-4 ">
                    <textarea type="text" name="message" id="message" class="form-control message-area"
                        placeholder="Message*"></textarea>
                </div>

                
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-2 mb-4">
                    <button type="submit" class="btn contactform-btn">Submit</button>
                    <div class="spinner-border text-danger" style="display: none"></div>

                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-2 mb-4">
                    <div class="alert alert-danger alert-dismissible" id="error_msg" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                        <span>Please Enter All Fields!</span>
                    </div>
                    <div class="alert alert-success alert-dismissible" id="success_msg" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-check"></i> Alert!</h5>
                        <span></span>
                    </div>
                </div>
            </div>
        </form>
    </div>
    @endsection

    @push("custom-css")
    <style>
        .about-caption.d-md-block {
            float: left;
            text-align: left;
            width: 70%;
            bottom: unset;
            top: 30%;
        }

        .about-caption.d-md-block h5 {
            font-size: 100px;
            color: #000;
            font-family: gmedium !important;
            font-weight: bold;
        }

        .about-caption {
            position: absolute;
            right: 15%;
            /* bottom: 20px; */
            left: 3%;
            z-index: 10;
            padding-top: 20px;
            padding-bottom: 20px;
            color: #fff;
            /* text-align: center; */
        }

        .about-heading {
            margin-top: 0;
            margin-bottom: .5rem;
            font-weight: 500;
            line-height: 1.6;
        }

        .cf-row {
            padding: 5px 50px 40px;
        }

        .column-lg {
            padding: 0 4rem;
        }

        .about_pg-para,
        .about_pg-para2 {
            font-family: 'cplight';
            font-size: 20px;
        }

        .about_pg-headng,
        .about_pg-headng2 {
            font-size: 62px;
            font-family: 'cpmedium';
            font-weight: 600;
        }

        .about_pg-para2,
        .about_pg-headng2 {
            text-align: right;
        }

        .about_pg-img {
            width: 370px;
            height: auto;
            display: block;
            margin: auto;
        }

        .getHeading {
            font-size: 72px;
            font-weight: 800;
            margin: auto;
            padding: 0 50px;
        }

        .submitbtnRow {
            padding-bottom: 20px;
        }

        .message-area {
            height: 200px !important;
        }

        .mi-row1 {
            padding: 60px 50px 60px 0;
        }

        .mi-row2 {
            padding: 30px 0 60px 50px;
        }
        @media(min-width:992px) and (max-width:1680px){
            .about_pg-headng , .about_pg-para {
                padding: 0 0 0 50px;
            }

            .about_pg-headng2 , .about_pg-para2 {
                padding: 0 50px 0 0;
            }
        }
        @media (max-width: 1680px) {
            .mi-row1 {
                padding: 60px 50px 60px;
            }

            .mi-row2 {
                padding: 60px 50px 60px;
            }
        }

        @media(max-width:1440px) {
            .mi-row1 {
                padding: 60px 50px 60px;
            }

            .mi-row2 {
                padding: 30px 50px 60px;
            }

            .about_pg-img {
                width: 300px;
            }
        }

        @media (max-width: 1280px) {
            .about_pg-img {
                width: 260px;
            }
        }

        @media (max-width: 1024px) {
            .mi-row1 {
                padding: 30px 40px 10px;
            }

            .mi-row2 {
                padding: 10px 40px 30px;
            }

            .about_pg-img {
                width: 200px;
            }

            .about_pg-headng,
            .about_pg-headng2 {
                font-size: 30px;
            }

            .about_pg-para,
            .about_pg-para2 {
                font-size: 15px;
            }

            .getHeading {
                font-size: 40px;
            }
        }

        @media(max-width:991px) {
            .mi-row2 {
                display: flex;
                flex-flow: column-reverse;
            }

            .mi-row1 {
                padding: 30px 40px 10px;
            }

            .mi-row2 {
                padding: 10px 40px 30px;
            }
        }

        @media(max-width:425px) {
            .mi-row1 {
                padding: 60px 15px 15px;
            }

            .mi-row2 {
                padding: 15px 15px 60px;
            }

            .about_pg-headng,
            .about_pg-headng2 {
                margin: 8px 0;
            }

            .about_pg-para,
            .about_pg-para2 {
                margin: 4px 0;
            }

            .mi-row2 {
                display: flex;
                flex-flow: column-reverse;
            }

            .cf-row {
                padding: 5px 15px 40px;
            }

            .getHeading {
                font-size: 36px;
                padding: 0 15px;
            }
        }

    </style>
    @endpush

    @push('custom-script')

    <script>

$(".contactform-btn").click(function (e) {
        e.preventDefault();

        var _token      = "{{ csrf_token() }}";
        let name        = $("#name").val();
        let email       = $("#email").val();
        let subject     = $("#subject").val();
        let message     = $("#message").val();
        var txt         = "";

        if( name == "" || email == "" || subject == "" || message == "")
        {
            // alert("Please Enter All Fields!");
            txt = "Please Enter All Fields!";
            $("#error_msg").find("span").text(txt);
            $('#error_msg').show();
        }
        else{
            // $(".contactform-btn").prop('disabled',true);
            $(".contactform-btn").css("display", "none");
            $(".spinner-border").show();

            $.ajax({
                type: "post",
                url: "{{ route('AjaxCallForInquiryForm') }}",
                data: {
                    _token  : _token,
                    name: name,
                    email: email,
                    subject: subject,
                    message: message,
                },
                success: function (response) {
                    if(response['status'])
                    {
                        txt = "Form is successfully sent!";
                        $("#success_msg").find("span").text(txt);
                        $('#success_msg').show();

                    }
                    else{
                        txt = "Form is not sent!";
                        $("#error_msg").find("span").text(txt);
                        $('#error_msg').show();
                    }

                    $("#name").val("");
                    $("#email").val("");
                    $("#subject").val("");
                    $("#message").val("");
                    // $(".contactform-btn").prop('disabled',false);
                    $(".spinner-border").hide();

                    $(".contactform-btn").show();

                }
            });

        }
        
    });

        // $(".contactform-btn").click(function (e) {
        //     e.preventDefault();

        //     let name = $("#name").val();
        //     let email = $("#email").val();
        //     let subject = $("#subject").val();
        //     let message = $("#message").val();

        //     $(".contactform-btn").prop('disabled', true);

        //     if (name == "" || email == "" || subject == "" || message == "") {
        //         alert('please fill all fields')
        //     } else {
        //         $.ajax({
        //             type: "POST",
        //             url: "{{route('AjaxCallForInquiryForm')}}",
        //             data: {
        //                 _token: "{{ csrf_token() }}",
        //                 name: name,
        //                 email: email,
        //                 subject: subject,
        //                 message: message,
        //             },
        //             success: function (response) {
        //                 alert(response['message']);
        //                 $("#name").val("");
        //                 $("#email").val("");
        //                 $("#subject").val("");
        //                 $("#message").val("");
        //                 $(".contactform-btn").prop('disabled', false);

        //             }
        //         });
        //     }

        // });

    </script>

    @endpush
