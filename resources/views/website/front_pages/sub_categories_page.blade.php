@extends('website.layouts.app')

@section('content')

<div id="iko5" class="hero-image-shop">
    <div class="hero-text">
        <h1 class="font_size70" id="ik9h">
            {{ @$heading->name }}
        </h1>
    </div>
</div>


@if (@count($data) > 0 && @count($product) > 0)
    @if ($heading->parent_id!=null)
        <div class="container mt-2 ml-5">
            <a href="{{route('website.subCategory',$heading->parent_id)}}">
                <!-- <i class="fas fa-arrow-circle-left"></i> -->
                <div class="backBtn">
                <span class="line tLine"></span>
                <span class="line mLine"></span>
                <span class="label">Back</span>
                <span class="line bLine"></span>
                </div>
            </a>
        </div>
    @else
        <div class="container mt-2 ml-5">
            <a href="{{route('website.shop')}}">
                <!-- <i class="fas fa-arrow-circle-left"></i> -->
                <div class="backBtn">
                <span class="line tLine"></span>
                <span class="line mLine"></span>
                <span class="label">Back</span>
                <span class="line bLine"></span>
                </div>
            </a>
        </div>
    @endif

<div class="shop-carousel">
    <div class="container">
        <h2 class="s-latest-product p1">{{ @$heading->name }} Categories</h2>
        <div class="owl-carousel reponsive owl-theme">
            @foreach ($data as $item)
            <div class="item">
                <a href="{{route('website.subCategory',$item->id)}}">
                    <img src="{{asset('backend/images/category').'/'.((@$item->image!="")? @$item->image:"dummy_image.png")}}" />
                    <h3 class="shop-carousel-title">{{@$item->name}}</h3>
                </a>
            </div>
            @endforeach

            <!-- <div class="owl-nav">
                <div class="owl-prev">prev</div>
                <div class="owl-next">next</div>
            </div> -->
        </div>
    </div>
</div>
@endif

@if (@count($data) > 0 && @count($product) == 0)
    @if ($heading->parent_id!=null)
        <div class="container mt-2 ml-5">
            <a href="{{route('website.subCategory',$heading->parent_id)}}">
                <!-- <i class="fas fa-arrow-circle-left"></i> -->
                <div class="backBtn">
                <span class="line tLine"></span>
                <span class="line mLine"></span>
                <span class="label">Back</span>
                <span class="line bLine"></span>
                </div>
            </a>
        </div>
    @else
        <div class="container mt-2 ml-5">
            <a href="{{route('website.shop')}}">
                <!-- <i class="fas fa-arrow-circle-left"></i> -->
                <div class="backBtn">
                <span class="line tLine"></span>
                <span class="line mLine"></span>
                <span class="label">Back</span>
                <span class="line bLine"></span>
                </div>
            </a>
        </div>
    @endif
<div class="container-fluid py-3 shop-sec-prod">
    <div class="container pt-1 custom-pad">
        <!-- <h2 class="s-latest-product text-center">Sub Categories</h2> -->
        @php
        $i = 0;
        @endphp
        <div class="row">
            @foreach ($data as $item)
            <div class="card py-3">
                <a href="{{route('website.subCategory',$item->id)}}" class="shop-p-link"><img
                        src="{{asset('backend/images/category').'/'.((@$item->image!="")? @$item->image:"dummy_image.png")}}" alt="image alt"
                        class="product-dynamic-images">
                    <span class=" display-1 text-center text-uppercase shop-carousel-title">{{@$item->name}}</span></a>
            </div>
            @php
            $i++;
            @endphp
            @endforeach
        </div>
    </div>
</div>

@elseif(@count($data) == 0 && @count($product) > 0)

@if ($heading->parent_id!=null)
        <div class="container mt-2 ml-5">
            <a href="{{route('website.subCategory',$heading->parent_id)}}">
                <!-- <i class="fas fa-arrow-circle-left"></i> -->
                <div class="backBtn">
                <span class="line tLine"></span>
                <span class="line mLine"></span>
                <span class="label">Back</span>
                <span class="line bLine"></span>
                </div>
            </a>
        </div>
    @else
        <div class="container mt-2 ml-5">
            <a href="{{route('website.shop')}}">
                <!-- <i class="fas fa-arrow-circle-left"></i> -->
                <div class="backBtn">
                <span class="line tLine"></span>
                <span class="line mLine"></span>
                <span class="label">Back</span>
                <span class="line bLine"></span>
                </div>
            </a>
        </div>
    @endif
<div class="container-fluid py-3 shop-sec-prod">
    <div class="container pt-1 custom-pad">
        <!-- <h2 class="s-latest-product text-center">Products</h2> -->
        @php
        $i = 0;
        @endphp
        <div class="row product_padding">
            @foreach ($product as $item)
            <div class="card py-3">
                <a href="{{route('website.productDetails',$item->id)}}" class="shop-p-link">
                    <img src="{{asset('backend/images/product').'/'.((@$item->image!="")? @$item->image:"dummy_image.png")}}"
                        alt="image alt" class="product-dynamic-images">
                    <span class=" display-1 text-center text-uppercase shop-carousel-title">{{@$item->name}}</span></a>
            </div>
            @php
            $i++;
            @endphp
            @endforeach
        </div>
    </div>
</div>
@elseif(@count($data) > 0 && @count($product) > 0)

<div class="container-fluid py-3 shop-sec-prod">
    <div class="container pt-1 custom-pad">
        <!-- <h2 class="s-latest-product text-center">Products</h2> -->
        @php
        $i = 0;
        @endphp
        <div class="row product_padding">
            @foreach ($product as $item)
            <div class="card py-3">
                <a href="{{route('website.productDetails',$item->id)}}" class="shop-p-link"><img src="{{asset('backend/images/product').'/'.((@$item->image!="")? @$item->image:"dummy_image.png")}}"
                        alt="image alt" class="product-dynamic-images">
                    <span class=" display-1 text-center text-uppercase shop-carousel-title">{{@$item->name}}</span></a>
            </div>
            @php
            $i++;
            @endphp
            @endforeach
        </div>
    </div>
</div>
@else

<div class="container-fluid py-3 shop-sec-prod">
    <div class="container pt-1 custom-pad no-cat-found">
        Record not Available!
    </div>
</div>

@endif






@endsection

@push("custom-css")
<link rel="stylesheet" href="{{asset('backend/plugins/owlcarousel/dist/assets/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/owlcarousel/dist/assets/owl.theme.default.min.css')}}">
<style>





    .no-cat-found {
        height: 125px;
        line-height: 100px;
        text-align: center;
        border: 2px dashed #c21d3b;
        font-size: 20px;
        font-family: 'cpmedium';
        text-transform: capitalize;
    }

    .fas.fa-arrow-circle-left {
        font-size: 50px;
        color: ;
        color: rgb(194, 29, 59);
    }
    .font_size70{
        font-size: 70px !important;
    }

</style>
@endpush
@push("custom-script")

<script>
    $(document).ready(function () {

        $(".owl-carousel").owlCarousel({
            loop: false,
            margin: 15,
            nav: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                },
                1200: {
                    items: 5
                }
            }
        });

    });

</script>

<script src="{{asset('backend/plugins/owlcarousel/dist/owl.carousel.min.js')}}"></script>
@endpush
