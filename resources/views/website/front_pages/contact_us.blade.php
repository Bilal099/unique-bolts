@extends('website.layouts.app')

@section('content')

<div id="iko5" class="hero-image-contact">
    <div class="hero-text">
        <h1 id="ik9h">CONTACT</h1>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-form">
            <h1 class="contact-headng">CONTACT FORM</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 col-form">
            <form name="contact" method="POST" data-netlify="true">
                <input type="text" name="name" id="name" placeholder="Name*" class="form-control mb-3" />
                <input type="text" name="email" id="email" placeholder="Email*" class="form-control mb-3" />
                <div class="row mb-1">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <input type="text" name="subject" id="subject"  placeholder="Company*" class="form-control mb-3" />
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <input type="text" name="phone" id="phone" placeholder="Phone*" class="form-control mb-3" />
                    </div>
                </div>
                <textarea id="textarea-contacts-01" name="message" rows="5" placeholder="Message*" class="form-control mb-3 inpt-message"></textarea>
                <div class="row mb-1">
                    <div class="col-md-6">
                        <div class="alert alert-danger alert-dismissible" id="error_msg" style="display: none">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                            <span>Please Enter All Fields!</span>
                        </div>
                        <div class="alert alert-success alert-dismissible" id="success_msg" style="display: none">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h5><i class="icon fas fa-check"></i> Alert!</h5>
                            <span></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <button type="submit" class="btn contactform-btn">Submit</button>
                        <div class="spinner-border text-danger" style="display: none"></div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 contact-details_map">
            <h5 id="i485k">YOU CAN FIND US HERE</h5>
            <p class="text-muted">Sample Address Here, 123 Stree A Block, Pakistan</p>
            {{-- <iframe frameborder="0" id="izx0s" src="https://maps.google.com/maps?&z=1&t=q&output=embed"></iframe> --}}
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d904.9146791557595!2d67.05268782923423!3d24.875503215685306!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjTCsDUyJzMxLjgiTiA2N8KwMDMnMTEuNyJF!5e0!3m2!1sen!2s!4v1631704144921!5m2!1sen!2s" width="400" height="400" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            <p id="ifful">Call: 123-123-123-456</p>
            {{-- <p id="ifful-2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;123-123-123-456</p> --}}
            <p></p>
        </div>
    </div>
</div>

@endsection

@push("custom-script")

<script>
    $(".contactform-btn").click(function (e) {
        e.preventDefault();

        var _token      = "{{ csrf_token() }}";
        var name        = $("#name").val();
        var email       = $("#email").val();
        var subject     = $("#subject").val();
        var phone       = $("#phone").val();
        var message     = $(".inpt-message").val();
        var txt         = "";

        if( name == "" || email == "" || subject == "" || phone == "" || message == "")
        {
            // alert("Please Enter All Fields!");
            txt = "Please Enter All Fields!";
            // $("#error_msg").find("span").text(txt);
            // $('#error_msg').show();
            $(document).Toasts('create', {
                class: 'bg-danger',
                title: 'Error Alert',
                // subtitle: 'Subtitle',
                body: txt
            })
        }
        else{
            // $(".contactform-btn").prop('disabled',true);
            // $(".contactform-btn").css("display", "none");
            $(".contactform-btn").hide();

            $(".spinner-border").show();

            $.ajax({
                type: "post",
                url: "{{ route('AjaxCallForContactUs') }}",
                data: {
                    _token  : _token,
                    name    : name,
                    email   : email,
                    subject : subject,
                    phone   : phone,
                    message : message,
                },
                success: function (response) {
                    if(response['status'])
                    {
                        txt = "Form is successfully sent!";
                        // $("#success_msg").find("span").text(txt);
                        // $('#success_msg').show();

                        $(document).Toasts('create', {
                            class: 'bg-success',
                            title: 'Success Alert',
                            // subtitle: 'Subtitle',
                            body: txt
                        });

                    }
                    else{
                        txt = "Form is not sent!";
                        // $("#error_msg").find("span").text(txt);
                        // $('#error_msg').show();

                        $(document).Toasts('create', {
                            class: 'bg-danger',
                            title: 'Error Alert',
                            // subtitle: 'Subtitle',
                            body: txt
                        })
                    }

                    $("#name").val("");
                    $("#email").val("");
                    $("#subject").val("");
                    $("#phone").val("");
                    $(".inpt-message").val("");
                    // $(".contactform-btn").prop('disabled',false);
                    $(".spinner-border").hide();

                    $(".contactform-btn").show();

                    if(response['status'])
                    {
                        $('#success_msg').fadeOut( "slow" );

                    }
                    else{
                        $('#error_msg').fadeOut( "slow" );
                    }

                }
            });

        }
        
    });

</script>

@endpush
