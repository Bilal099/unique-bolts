@extends('website.layouts.app')



@section('content')

<div id="iko5" class="hero-image-shop">
    <div class="hero-text">
        {{-- class="font_size70" --}}
        <h1 class="font_size70" id="ik9h">Product Detail</h1> 
    </div>
</div>

@if (isset($product))
<div class="container mt-2 ml-5">
    <a href="{{route('website.subCategory',$product->category_id)}}">
        <!-- <i class="fas fa-arrow-circle-left"></i> -->
        <div class="backBtn">
      <span class="line tLine"></span>
      <span class="line mLine"></span>
      <span class="label">Back</span>
      <span class="line bLine"></span>
	  </div>
    </a>
</div>
@endif


@if(@$product != null)

<div class="container">
    <div class="row mi-row1">
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12 mt-5 mb-4">
            <img src="{{ @asset('backend/images/product')."/".((@$product->image!="")? @$product->image:"dummy_image.png")}}" class="shopDetails_pg-img">
        </div>
        <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-xs-12 m-auto">
            <h2 class="shopDetails_pg-headng">{{ @$product->name }}</h2>
            
                <p class="shopDetails_pg-para">
                    {!! @$product->description !!}
                </p>
        </div>
    </div>
    @if (count($product->ProductImage) > 1)
    <hr>
    <h2 class="" style="font-weight:900">Detailing Images</h2>
    <div class="row mi-row1">

        <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-xs-12 m-auto sub-images">
        @foreach ($product->ProductImage as $item)
            <div class="mySlides">
                <img src="{{ @asset('backend/images/productImage')."/".((@$item->image!="")? @$item->image:"dummy_image.png")}}" height="400" width="auto" alt="" srcset="">     
            </div>
        @endforeach
        <div class="row mt-5" style="justify-content: center;">

                @foreach ($product->ProductImage as $key => $item)

                    <div class="column">
                        <img class="demo cursor" src="{{ @asset('backend/images/productImage')."/".((@$item->image!="")? @$item->image:"dummy_image.png")}}" height="50" width="auto" onclick="currentSlide({{$key+1}})">
                    </div>
                    {{-- <div class="mySlides">
                        <img src="{{ @asset('backend/images/productImage')."/".((@$item->image!="")? @$item->image:"dummy_image.png")}}" height="150" width="auto" alt="" srcset="">     
                    </div> --}}
                @endforeach
        </div>
        </div>
        
    </div>
    @elseif (count($product->ProductImage) == 1)
    <hr>
    <h2 class="" style="font-weight:900">Detailing Image</h2>
    <div class="row mi-row1">

        <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-xs-12 m-auto sub-images">
            <img src="{{ @asset('backend/images/productImage')."/".((@$product->ProductImage[0]->image!="")? @$product->ProductImage[0]->image:"dummy_image.png")}}" height="400" width="auto" alt="" srcset="">     
        </div>
    </div>
    @endif

</div>


@else

<div class="container-fluid py-3 shop-sec-prod">
    <div class="container pt-1 custom-pad no-cat-found">
        Record not Available!
    </div>
</div>

@endif


@endsection
@push("custom-css")
<style>

div.backBtn {
  width: 100px;
  left: 100px;
  /* top: 100px; */
  top:60%;
  background-color: #f4f4f4;
  transition: all 0.4s ease;
  /* position: fixed; */
  position:absolute;
  cursor: pointer;
}

span.line {
  bottom: auto;
  right: auto;
  top: auto;
  left: auto;
  background-color:rgb(194, 29, 59); ;
  border-radius: 10px;
  width: 100%;
  left: 0px;
  height: 2px;
  display: block;
  position: absolute;
  transition: width 0.2s ease 0.1s, left 0.2s ease, transform 0.2s ease 0.3s, background-color 0.2s ease;
}

span.tLine {
  top: 0px;
}

span.mLine {
  top: 13px;
  opacity: 0;
}

span.bLine {
  top: 26px;
}

.label {
  /* top: 5px; */
  /* font-size: 1em;  */
   position: absolute;
    left: 0px;
    top: 0px;
    width: 100%;
    text-align: center;
    transition: all 0.4s ease;
    font-size: 18px;
    color: black;
    font-weight: bold;
}

div.backBtn:hover span.label {
  left: 25px
}

div.backBtn:hover span.line {
  left: -10px;
  height: 5px;
  background-color: rgb(194, 29, 59);;
}

div.backBtn:hover span.tLine {
  width: 25px;
  transform: rotate(-45deg);
  left: -15px;
  top: 6px;
}

div.backBtn:hover span.mLine {
  opacity: 1;
  width: 30px;
}

div.backBtn:hover span.bLine {
  width: 25px;
  transform: rotate(45deg);
  left: -15px;
  top: 20px;
}


    .fas.fa-arrow-circle-left {
        font-size: 50px;
        color: ;
        color: rgb(194, 29, 59);
    }

    .btn-rounded {
        border-radius: 50px
    }

    .sub-images{
        margin: 0 auto !important;
        text-align: center;
    }
    .mySlides {
        display: none;
    }
    .column {
        float: left;
        width: 16.66%;
    }

    .font_size70{
        font-size: 70px !important;
    }
    .no-cat-found {
        height: 125px;
        line-height: 100px;
        text-align: center;
        border: 2px dashed #c21d3b;
        font-size: 20px;
        font-family: 'cpmedium';
        text-transform: capitalize;
    }
    .mi-row1 {
        padding: 40px 60px;
    }
    .shopDetails_pg-img{
        width: 450px;
        height: 450px;
        margin: auto;
        display: block;
        border: 7px solid red;
        border-radius: 50%;
        padding: 40px 30px;
        box-shadow: 0px 0px 20px 10px red;
        object-fit: scale-down;
    }
    .shopDetails_pg-headng {
        font-size: 62px;
        font-family: 'cpmedium';
        font-weight: 600;
        color: rgb(224, 28, 42);
        position: relative;
        box-sizing: border-box;
        margin: auto;
        background: white;
        padding-bottom: 6px;
        
    }
    .shopDetails_pg-headng:after {
        content: '';
        position: absolute;
        top: 2px;
        right: 0;
        bottom: 0;
        left: 1px;
        z-index: -1;
        margin-bottom: -3px;
        background: linear-gradient(0.25turn, rgb(150 150 137), rgb(64 59 61), rgba(56,2,155,0));
    }
    .shopDetails_pg-para{
        font-family: 'cplight';
        font-size: 20px; 
    }
    @media(max-width:1600px){
        .shopDetails_pg-img{
            width: 300px;
            height: 300px;
        }
    }
    @media(max-width:1440px){
        .shopDetails_pg-img{
            width: 300px;
            height: 300px;
        }
    }
    @media(max-width:1280px){
        .shopDetails_pg-headng , .shopDetails_pg-para{
            padding-left: 30px;
        }
    }
    @media(max-width:1024px){
        .shopDetails_pg-img {
            width: 200px;
            height: 200px;
        }
        .shopDetails_pg-headng {
            font-size: 30px;
        }
        .shopDetails_pg-para {
            font-size: 15px;
        }
    }
    @media(max-width:768px){
        .mi-row1 {
            padding: 20px 20px;
        }
        .shopDetails_pg-headng, .shopDetails_pg-para{
            padding: 0;
        }
    }
</style>
@endpush

@push('custom-script')
<script>
    var slideIndex = 1;
    showSlides(slideIndex);
    
    // function plusSlides(n) {
    //   showSlides(slideIndex += n);
    // }
    
    function currentSlide(n) {
      showSlides(slideIndex = n);
    }
    
    function showSlides(n) {
      var i;
      var slides = document.getElementsByClassName("mySlides");
      var dots = document.getElementsByClassName("demo");
    //   var captionText = document.getElementById("caption");
      if (n > slides.length) {slideIndex = 1}
      if (n < 1) {slideIndex = slides.length}
      for (i = 0; i < slides.length; i++) {
          slides[i].style.display = "none";
      }
      for (i = 0; i < dots.length; i++) {
          dots[i].className = dots[i].className.replace(" active", "");
      }
      slides[slideIndex-1].style.display = "block";
      dots[slideIndex-1].className += " active";
    //   captionText.innerHTML = dots[slideIndex-1].alt;
    }
    </script>
@endpush