<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="None">
    <meta name="author" content="">
	<title>Unique Bolts</title>
	<link rel="icon" type="image/png" href="{{asset('backend/dist/img/icon.png')}}"/>
	@include('website.partials.style')
</head>
<body>
<header role="banner">
<nav class="navbar navbar-expand-lg navbar-light" style="background-color: transparent;">
    <a class="navbar-brand d-lg-none" href="#"><img src="{{asset('frontend/images/logo.png')}}"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myNavbarToggler7"
        aria-controls="myNavbarToggler7" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="myNavbarToggler7">
        <ul class="navbar-nav mx-auto custom-nav-width">
			<li class="nav-item">
				<form action="{{route('SearchProductDetail')}}" method="POST">
					@csrf
					<div class="p-1 search-input-grp rounded rounded-pill mb-4">
						<div class="input-group" style="height:21px;">
						<input name="search" type="search" placeholder="SEARCH" aria-describedby="button-addon1" class="form-control border-0 bg-light search-input" id="search_btn" autocomplete="off">
						
						<div class="input-group-append">
							<button id="button-addon1" type="submit" class="btn btn-link text-primary"><i class="fa fa-search"></i></button>
						</div>
						</div>
						
					</div>
					<div id="search_list" class="list-group" style="position: absolute; z-index: 111; max-height: 500px; margin-top: -24px; overflow-y: scroll;display:none">
					</div>
				</form> 
			</li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('/')}}">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('/about_us')}}">About</a>
            </li>
            <a class="d-none d-lg-block" href="{{url('/')}}"><img src="{{asset('website/images/Unique-logo.png')}}"></a>
            <li class="nav-item">
                <a class="nav-link" href="{{url('/shop')}}">Shop</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('/contactUs')}}">Contact</a>
            </li>
			<li class="nav-item">
				<a class="nav-link"  href="#">
					{{-- <button class="btn mr-2 signin-btn" >SIGN IN
					</button>
					CART<i class="fa fa-shopping-cart mr-2 " aria-hidden="true"></i>
					<span class="badge ">2</span> --}}
					<div style="margin: auto;width:200px"></div>
				</a>
			</li>
        </ul>
    </div>
</nav>
</header><!-- header role="banner" -->
@yield('content')

@php
	$our_partners = App\Http\Controllers\website\HomeController::getPartners();
	$count = 1;
@endphp
<footer>
	<div id="footer-main">
		<div class="container-fluid">
			<div class="container-fliud-w-padding">
				<div class="row">
					<div class="col">
						<h4>Our Partners</h4>
						<div class="partners-web-col">
							@foreach ($our_partners as $item)
								@if ($count == 1 || $count == 6)
									@if ($count != 1)
										</ul>
									@endif
									<ul>
								@endif
								<li><a href="{{@$item->link}}" target="_blank">{{@$item->name}}</a></li>

								@if ($count == 10)
									@break
								@endif
								@php
									$count++;
								@endphp
							@endforeach
							{{-- <ul>
								<li><a href="#">Website 1</a></li>
								<li><a href="#">Website 2</a></li>
								<li><a href="#">Website 3</a></li>
								<li><a href="#">Website 4</a></li>
								<li><a href="#">Website 5</a></li>
							</ul>
							<ul>
								<li><a href="#">Website 6</a></li>
								<li><a href="#">Website 7</a></li>
								<li><a href="#">Website 8</a></li>
								<li><a href="#">Website 9</a></li>
								<li><a href="#">Website 10</a></li>
							</ul> --}}
						</div>
					</div>
					<div class="col">
						<h4>Company Information</h4>
							<ul>
								<li><a href="{{url('/about_us')}}">About Us</a></li>
								<li><a href="#">Careers</a></li>
								<li><a href="#">Marketing Partners</a></li>
								<li><a href="#">Invvestors Relations</a></li>
								<li><a href="#">Legal Information</a></li>
							</ul>
					</div>
					<div class="col">
						<h4>Customer Service</h4>
							<ul>
								<li><a href="{{url('/contactUs')}}">Contact Us</a></li>
								{{-- <li><a href="#">Feedback</a></li> --}}
								<li><a href="#">Branch Locator</a></li>
							</ul>
					</div>
					<div class="col col-the-last">
						<h4>Stay in the Know</h4>
						<p class="sign-up-text">Stay with us for the latest deals and our free magazine</p>
						{{-- <a href="#" class="accept-btn">Accept and Continue</a> --}}
						<h3>Need Assistance? Call 123-456-789</h3>
						<div>
							<h2>We are where we are.</h2>
							<p class="copyright-text">Copyright 2021, All Rights Reserved.</p>
					</div>
				</div>				
			</div>
		</div>
	</div>
</footer>
@include('website.partials.script')
<script>

	var str = "";
	$("#search_btn").keyup(function (e) { 
		e.preventDefault();
		let val = $(this).val();
		$.ajax({
			type: "post",
			url: "{{route('AjaxCallForSearchProducts')}}",
			data:{ 
				_token : '<?php echo csrf_token() ?>',
                value : val,
            },
			success: function (response) {
				$('#search_list').html("");
				str = "";

				if (!response) {
					$('#search_list').hide();
					$('#search_list').html("");
				} 
				else {
					$.each(response,function(key,val){
						str += `<a  href="/productDetails/${val.id}" class="list-group-item">${val.name}</a>`
					});
					if(response.length >0){
						$('#search_list').html(str);
						$('#search_list').show();
					}
					else{
						$('#search_list').hide();
						$('#search_list').html("");
					}
				}
				
			}
		});

	});

	$("#iko5").click(function(){
		$("form").find('#search_list').hide();
  	});
	  $(".container").click(function(){
		$("form").find('#search_list').hide();
  	});

	$(".nav-item").on("click", "#search_list a", function(event){
		$(this).show();
	});

</script>


</body>

