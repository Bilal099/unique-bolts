<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('AjaxCallForSearchProducts','Admin\SearchController@AjaxCallForSearchProducts')->name('AjaxCallForSearchProducts'); // Ajax call for search input
Route::post('search','Admin\SearchController@SearchProductDetail')->name('SearchProductDetail'); // for search Product Detail

Route::get('/',"website\HomeController@index")->name('home');
Route::get('/about_us',"website\HomeController@about_us")->name('about_us');
Route::get('/shop',"website\HomeController@shop")->name('website.shop');
Route::get('/productDetails/{id}',"website\HomeController@productDetails")->name('website.productDetails');


Route::get('/login',"website\AuthController@loginShow")->name('login')->middleware('guest:web');
Route::post('/login',"website\AuthController@loginPost")->name('login.store');
Route::get('/logout',"website\AuthController@logout")->name('logout')->middleware('auth:web');

Route::get('/contactUs',"website\FrontPageController@viewContactUs")->name('contactUs');
Route::post('/send-ContactUs',"website\FrontPageController@AjaxCallForContactUs")->name('AjaxCallForContactUs');

Route::get('/inquiryForm',"website\FrontPageController@viewInquiryForm")->name('inquiryForm');
Route::post('/send-InquiryForm',"website\FrontPageController@AjaxCallForInquiryForm")->name('AjaxCallForInquiryForm');

Route::get('/subCategory/{id}',"website\HomeController@sub_category")->name('website.subCategory');
Route::get('/categoryProducts/{id}',"website\HomeController@category_products")->name('website.categoryProducts');




Route::middleware('auth:web')->group(function () {
    Route::get('home',"website\HomeController@index")->name('home');
});


Route::get('/admin/login',"Admin\AuthController@loginShow")->name('admin.login')->middleware('guest:admin');
Route::post('/admin/login',"Admin\AuthController@loginPost")->name('admin.login.store');
Route::get('/admin/logout',"Admin\AuthController@logout")->name('admin.logout')->middleware('auth:admin');


Route::get('/forgotpassword/{type}',            "ForgetPasswordResetController@viewForgotpassword")                 ->name('forgotpassword');
Route::get('/resetPassword/{email_encode}',     "ForgetPasswordResetController@ResetPassword")                      ->name('resetPassword'); // Forgot Password send email 
Route::post('/SetNewPassword',                  "ForgetPasswordResetController@SetNewPassword")                     ->name('SetNewPassword'); // Set New Password 
Route::post('/AjaxCallForForgotPasswordEmail',  "ForgetPasswordResetController@AjaxCallForForgotPasswordEmail")     ->name('AjaxCallForForgotPasswordEmail'); // Forgot Password send email 

Route::get('register',"website\RegisterController@viewRegister")->name("register");
Route::post('register',"website\RegisterController@registerUser")->name("registerUser");



Route::middleware('auth:admin')->group(function () {
    Route::get('admin/home',"Admin\HomeController@index")->name('admin.home');

    Route::get('admin/ContactUs',                           "Admin\CMSController@viewContactUS")                    ->name('admin.ContactUs');
    Route::post('admin/AjaxCallForSaveContactUsMetaData',   "Admin\CMSController@AjaxCallForSaveContactUsMetaData") ->name('AjaxCallForSaveContactUsMetaData');
    Route::post('admin/AjaxCallForViewContactUs',           "Admin\CMSController@AjaxCallForViewContactUs")         ->name('AjaxCallForViewContactUs');

    Route::get('admin/InquiryForm',                           "Admin\CMSController@viewInquiryForm")                    ->name('admin.InquiryForm');
    Route::post('admin/AjaxCallForSaveInquiryFormMetaData',   "Admin\CMSController@AjaxCallForSaveInquiryFormMetaData") ->name('AjaxCallForSaveInquiryFormMetaData');
    Route::post('admin/AjaxCallForViewInquiryForm',           "Admin\CMSController@AjaxCallForViewInquiryForm")         ->name('AjaxCallForViewInquiryForm');

    Route::get( 'admin/AboutUs',        "Admin\CMSController@AboutUs")                  ->name('admin.aboutUs');
    Route::post('admin/AboutUs/store',  "Admin\CMSController@AboutUsCreateOrUpdate")    ->name('admin.aboutUs.store');


    Route::get('admin/categories',              "Admin\CategoryController@index")   ->name('admin.category');
    Route::get('admin/categories/create',       "Admin\CategoryController@create")  ->name('admin.categoryCreate');
    Route::post('admin/categories/store',       "Admin\CategoryController@store")   ->name('admin.categoryStore');
    Route::get('admin/categories/edit/{id}',    "Admin\CategoryController@edit")    ->name('admin.categoryEdit');

    Route::get( 'admin/product',                "Admin\ProductController@index")                            ->name('admin.product');
    Route::get( 'admin/product/create',         "Admin\ProductController@create")                           ->name('admin.productCreate');
    Route::post('admin/product/store',          "Admin\ProductController@store")                            ->name('admin.productStore');
    Route::get( 'admin/product/edit/{id}',      "Admin\ProductController@edit")                             ->name('admin.productEdit');
    Route::get( 'admin/product/delete/{id}',    "Admin\ProductController@destroy")                          ->name('admin.productDelete');
    Route::post('admin/productImage/delete',    "Admin\ProductController@AjaxCallForDeleteProductImage")    ->name('AjaxCallForDeleteProductImage');



    Route::get( 'admin/partner',            "Admin\PartnerController@index")    ->name('admin.partner');
    Route::get( 'admin/partner/create',     "Admin\PartnerController@create")   ->name('admin.partnerCreate');
    Route::post('admin/partner/store',      "Admin\PartnerController@store")    ->name('admin.partnerStore');
    Route::get( 'admin/partner/edit/{id}',  "Admin\PartnerController@edit")     ->name('admin.partnerEdit');
    Route::post('admin/partner/delete',     "Admin\PartnerController@destroy")  ->name('admin.partnerDelete');

    Route::post('admin/product/import',      "Admin\ProductController@importProducts")     ->name('admin.productImport');
    Route::get('admin/product/import',      "Admin\ProductController@importView")     ->name('admin.productImportView');



});

