<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categories";
    
    public function parent()
    {
        return $this->belongsTo('App\Models\Category','parent_id', 'id');
    }

    public function parent_relation()
    {
        return $this->hasMany('App\Models\Category','parent_id', 'id');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product','category_id', 'id');
    }
}
