<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Product extends Model
{
    use SoftDeletes;
    protected $table = "products";

    public function category()
    {
        return $this->belongsTo('App\Models\Category','category_id', 'id');
    }

    public function ProductImage()
    {
        return $this->hasMany('App\Models\ProductImage','product_id', 'id');
    }
}
