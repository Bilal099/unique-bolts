<?php

namespace App\Imports;

use App\Models\Product;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ProductImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        // $heading = ['Product Name', 'Discriptive Name', 'Standard', 'Art No', 'Size Range', 'Material Available', 'Material', 'Imperial', 'Surface Finishing', 'Finishing Available', 'Description', 'Images Available' ];
        
        $heading = ['Product Name'	,'Application'	,'Benefits'	,'Description',	'Rolls & bats',	'Faced & unfaced',	'Use'];

        
        $str = "";
        // dd($collection[1][2]);
        // $standard = explode("•",$collection[1][2]);
        // dd($standard);
        $i = 0;
        $name_index = 0;
        $image_index = 7;
        $parent_category_id = 9;
        // $cat_id1 = 2;   // bolts
        // $cat_id2 = 3;   // screws
        // $cat_id3 = 6;   // nuts
        // $cat_id4 = 7;   // washers



        foreach ($collection as $row) 
        {
            $product       = new Product;
            if($i != 0)
            {
                foreach ($row as $key => $value) 
                {
                    if($row[$name_index] != "")
                    {  
                        if($key != $image_index)
                        {
                            if($key != $name_index)
                            {
                                if($value != "")
                                {
                                    $standard = explode("•",$value);

                                    $str .= '<p><b>'. $heading[$key].':</b></p>';
                                    $str .= '<ul>';
                                    foreach ($standard as $val)
                                    {
                                        $str .= '<li><span style="font-size: 1rem;">'.$val.'</span></li>';
                                    }
                                    $str .= '</ul>';
                                }
                            }
                        }
                        
                    }
                }

                // if(strpos(strtolower($row[$name_index]),'bolt'))
                // {
                //     $product->category_id   = $cat_id1;
                // }
                // elseif (strpos(strtolower($row[$name_index]),'screw')) 
                // {
                //     $product->category_id   = $cat_id2;
                // }
                // elseif (strpos(strtolower($row[$name_index]),'nuts')) 
                // {
                //     $product->category_id   = $cat_id3;
                // }
                // elseif (strpos(strtolower($row[$name_index]),'washers')) 
                // {
                //     $product->category_id   = $cat_id4;
                // }
                // else{
                //     $product->category_id   = $parent_category_id;
                // }

                $product->category_id   = $parent_category_id; // for only one category

                $product->name          = $row[$name_index];
                $product->description   = $str;
                if($row[$image_index] != "")
                {
                    $product->image    = $row[$image_index];
                }
                $product->save();
            }
            $i++;
            $str = "";
            // if($i == 5)
            // {
            //     break;
            // }
            
        }


    }
}
