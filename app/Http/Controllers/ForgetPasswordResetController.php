<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ForgotPassword;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Admin;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;


class ForgetPasswordResetController extends Controller
{
    public function viewForgotpassword($type)
    {
        if($type == "admin")
        {
            return view('admin.auth.forgotPassword');
        }
        elseif($type == "user")
        {
            return view('website.auth.forgotPassword');
        }
    }

    public function AjaxCallForForgotPasswordEmail(Request $request)
    {
        $to_email       = $request->email;
        $main_type      = $request->main_type;
        $email_encode   = $to_email."-".date_format(NOW(),'Ymd')."-".$main_type;

        for ($i=0; $i < 3; $i++) { 
            $email_encode = base64_encode($email_encode);
        }
        $detail = array();

        if($main_type == "admin")
        {
            $admin  = Admin::where('email',$to_email)->first();
            if($admin != null)
            {
                Mail::to($to_email)->send(new ForgotPassword($email_encode,$main_type));
                $detail[0] = true;
                $detail[1] = "Email is Succesfully send!";
                return $detail;
            }
            else{
                $detail[0] = false;
                $detail[1] = "Invalid Email!";
                return $detail;
            }
        }
        elseif($main_type == "user")
        {
            $User  = User::where('email',$to_email)->first();
            if($User != null)
            {
                Mail::to($to_email)->send(new ForgotPassword($email_encode,$main_type));
                $detail[0] = true;
                $detail[1] = "Email is Succesfully send!";
                return $detail;
            }
            else{
                $detail[0] = false;
                $detail[1] = "Invalid Email!";
                return $detail;
            }
        }
        
    }

    public function ResetPassword($email_encode)
    {
        $email_decode = $email_encode;
        for ($i=0; $i < 3; $i++) { 
            $email_decode = base64_decode($email_decode);
        }
        $email_decode   = explode('-',$email_decode);
        $email          = $email_decode[0];
        $expire_time    = $email_decode[1];
        $main_type      = $email_decode[2];
        
        if($expire_time == date_format(NOW(),'Ymd'))
        {
            if($main_type == "admin")
            {
                return view('admin.auth.resetPassword',compact('email'));
            }
            elseif($main_type == "user")
            {
                return view('website.auth.resetPassword',compact('email'));
                
            }

        }
        else{
            return view('linkExpirePage');
        }
    }

    public function SetNewPassword(Request $request)
    {
        $request->validate([
            'password'              => 'min:8|required_with:confirm_password|same:confirm_password',
            'confirm_password'      => 'min:8',
        ],[
            'password.required'              => 'This field is required',
            'confirm_password.required'      => 'This field is required',
        ]);

        try {

            if($request->password_type == "admin")
            {
                $admin  = Admin::where('email',$request->email)->first();
                if($admin != null)
                {
                    $admin->password = Hash::make($request->password);
                    $admin->save();
                }
            }
            elseif($request->password_type == "user")
            {
                $User  = User::where('email',$request->email)->first();
                if($User != null)
                {
                    $User->password = Hash::make($request->password);
                    $User->save();
                }
            }
        } 
        catch (\Throwable $th) {
            return redirect()->back()->with('message',$th);
        }

        if($request->password_type == "admin")
        {
            return Redirect::to('/admin/login')->with('success','Your password reset successfully!');   
        }
        elseif($request->password_type == "user")
        {
            return Redirect::to('/login')->with('success','Your password reset successfully!');
        }
    }
}
