<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index()
    {
        $data = Category::all();
        return view('admin.categories.index',compact('data'));
    }

    public function create()
    {
        $category_parent = Category::all();
        return view('admin.categories.create',compact('category_parent'));
    }

    public function store(Request $request)
    {
        // dd($request);
        if(isset($request->id))
        {
            $request->validate([
                'name'              => 'required',
                // 'featured_category' => 'nullable',
            ]);
            $success_msg    = "Data is Successfully Updated.";
            $category       = Category::find($request->id);
        }
        else{
            
            $request->validate([
                'name'              => 'required',
                // 'featured_category' => 'nullable',
            ]);
            $success_msg    = "Data is Successfully Added.";
            $category       = new Category;
            
        }
        try{
            

            if($request->hasFile('image'))
            {
                // if(isset($request->id))
                // {
                //     $file           = $category->image;
                //     $image_path     = public_path('/backend/images/category').'/'.$file;
                //     unlink($image_path);
                // }
                $imageName          = time().'.'.$request->image->extension();
                $request->image     ->move(public_path('/backend/images/category'), $imageName);
                $category->image    = $imageName;
            }
            
            $category->name                 = $request->name;
            $category->parent_id            = @$request->parent_id;
            $category->featured_category    = $request->has('featured_category')?1:0;
            $category->save();
        }
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Some thing is missing!');
        }

        return \redirect()->route('admin.category')->with('success',$success_msg);
    }

    public function edit($id)
    {
        $data = Category::find($id);
        $category_parent = Category::all();
        return view('admin.categories.create',compact('data','category_parent'));
    }
}
