<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;

class SearchController extends Controller
{
    public function AjaxCallForSearchProducts(Request $request)
    {
        if(!empty($request->value))
        {
            $Product = Product::where('name', 'like', '%'.$request->value.'%')->get();

            return $Product->all();
        }
        else
        {
            return false;
        }
        
    }

    public function SearchProductDetail(Request $request)
    {
        // dd($request);
        
        $Product = Product::where('name', 'like', '%'.$request->search.'%')->first();
        // dd($Product);
        if($Product != null)
        {
            return redirect()->route('website.productDetails',$Product->id);
        }
        else{
            return redirect()->back();
        }
    }
}
