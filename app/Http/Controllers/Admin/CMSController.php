<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ContactUs;
use App\Models\Inquiry;
use App\Models\FrontPage;
use Auth;

class CMSController extends Controller
{
    public function viewContactUS()
    {
        $data = ContactUs::all()->sortByDesc('id');
        $FrontPage  = FrontPage::where('slug','contact-us')->first();
        return view('admin.cms.contactUs.index',compact('data','FrontPage'));
    }

    public function AjaxCallForSaveContactUsMetaData(Request $request)
    {
        try {
            $page_detail    = json_encode("There is no data of contact us in this table.");
            $contactUs        = FrontPage::where('slug','contact-us')->first();

            $FrontPage                      = ($contactUs != null)? FrontPage::find($contactUs->id):new FrontPage;
            $FrontPage->title               = $request->title;
            $FrontPage->meta_title          = isset($request->meta_title)? $request->meta_title:'';
            $FrontPage->meta_keywords       = isset($request->meta_keyword)? $request->meta_keyword:'';
            $FrontPage->meta_description    = isset($request->meta_description)? $request->meta_description:'';
            $FrontPage->content             = $page_detail;
            $FrontPage->active              = 1;
            if($contactUs == null)
            {
                $FrontPage->slug            = 'contact-us'; 
                $FrontPage->created_by      = Auth::user()->id;
                $FrontPage->updated_by      = Auth::user()->id;
            }
            else{
                $FrontPage->updated_by      = Auth::user()->id;
            }
            $FrontPage->save();
            $messege = ($contactUs != null)? "Contact Us Details Updated Successfully":"Contact Us Details Created Successfully";

            return response()->json(['status' => true,'msg' => $messege]);
        } catch (\Throwable $th) {
            return response()->json(['status' => false,'msg' => $th]);

        }
    }

    public function AjaxCallForViewContactUs(Request $request)
    {
        $contactUs = ContactUs::find($request->id);
        return view('admin.cms.contactUs.viewModal',compact('contactUs'));
    }

    public function AboutUs()
    {
        $FrontPage  = FrontPage::where('slug','about-us')->first();
        if($FrontPage != null){
            $content    = json_decode($FrontPage->content);
            // dd($content);
        }
        else{
            $content = null;
        }
        return view('admin.cms.aboutUs.create',compact('FrontPage','content'));
    }

    public function AboutUsCreateOrUpdate(Request $request)
    {
        // dd($request->all());
        // dd(isset($request->section1_image));

        $aboutUs        = FrontPage::where('slug','about-us')->first();
        if($aboutUs != null)
        {
            $request->validate([
                'title'                     => 'required',
                'section1_title'            => 'required',
                'section1_description'      => 'required',
                'section2_title'            => 'required',
                'section2_description'      => 'required',
            ],[
                'title.required'                    => 'This field is required',
                'section1_title.required'           => 'This field is required',
                'section1_description.required'     => 'This field is required',
                'section2_title.required'           => 'This field is required',
                'section2_description.required'     => 'This field is required',
            ]);
        }
        else{
            $request->validate([
                'title'                     => 'required',
                'section1_title'            => 'required',
                'section1_image'            => 'required|image',
                'section1_description'      => 'required',
                'section2_title'            => 'required',
                'section2_image'            => 'required|image',
                'section2_description'      => 'required',
            ],[
                'title.required'                    => 'This field is required',
                'section1_title.required'           => 'This field is required',
                'section1_image.required'           => 'This field is required',
                'section1_description.required'     => 'This field is required',
                'section2_title.required'           => 'This field is required',
                'section2_image.required'           => 'This field is required',
                'section2_description.required'     => 'This field is required',
            ]);
        }
        
        

        // try {
           
            $aboutUs        = FrontPage::where('slug','about-us')->first();
            $FrontPage      = ($aboutUs != null)? FrontPage::find($aboutUs->id):new FrontPage;

            if($aboutUs != null)
            {
                $contect = json_decode($FrontPage->content);
                // dd($contect->section1_image);
            }

            // if($request->hasFile('section1_image'))
            if(isset($request->section1_image))
            {
                if($aboutUs != null)
                {
                    $file1           = $contect->section1_image;
                    $image_path1     = public_path('/backend/images/aboutUs').'/'.$file1;
                    if($contect->section1_image != "")
                    {
                        // dd("abc");
                        unlink($image_path1);

                    }
                    
                }
                $imageName1          = '1'.time().'.'.$request->section1_image->extension();
                $request->section1_image     ->move(public_path('/backend/images/aboutUs'), $imageName1);
                $section1_image    = $imageName1;
            }

            // if($request->hasFile('section2_image'))
            if(isset($request->section2_image))
            {
                if($aboutUs != null)
                {
                    $file2           = $contect->section2_image;
                    $image_path2     = public_path('/backend/images/aboutUs').'/'.$file2;
                    if($contect->section2_image != "")
                    {
                        // dd($request->hasFile('section2_image'));

                        unlink($image_path2);

                    }
                    
                }
                $imageName2          = '2'.time().'.'.$request->section2_image->extension();
                $request->section2_image     ->move(public_path('/backend/images/aboutUs'), $imageName2);
                $section2_image    = $imageName2;
            }

            // dd($section2_image."/".$section1_image);
        
            $temp = array(
                'section1_title'           => $request->section1_title,
                'section1_description'     => $request->section1_description,
                'section2_title'           => $request->section2_title,
                'section2_description'     => $request->section2_description,
                'section1_image'            => ($request->hasFile('section1_image'))? $section1_image:(($aboutUs != null)? (($contect->section1_image != "")? $contect->section1_image:""):"" ),
                'section2_image'            => ($request->hasFile('section2_image'))? $section2_image:(($aboutUs != null)? (($contect->section2_image != "")? $contect->section2_image:""):"" ),
                // 'section3_title'           => $request->section3_title,
                // 'section3_description'     => $request->section3_description,
            );

            $page_detail    = json_encode($temp);

            $FrontPage->title               = $request->title;
            $FrontPage->meta_title          = isset($request->meta_title)? $request->meta_title:'';
            $FrontPage->meta_keywords        = isset($request->meta_keyword)? $request->meta_keyword:'';
            $FrontPage->meta_description    = isset($request->meta_description)? $request->meta_description:'';
            $FrontPage->content             = $page_detail;
            $FrontPage->active              = 1;

            if($aboutUs == null)
            {
                $FrontPage->slug            = 'about-us'; 
                $FrontPage->created_by      = Auth::user()->id;
                $FrontPage->updated_by      = Auth::user()->id;

            }
            else{
                $FrontPage->updated_by      = Auth::user()->id;
            }
            // dd("1111");

            $FrontPage->save();

            $messege = ($aboutUs != null)? "About Us Updated Successfully":"About Us Created Successfully";
            return redirect()->route('admin.aboutUs')->with('success',$messege);
        // } catch (\Throwable $th) {
        //     return redirect()->route('admin.aboutUs')->with('error',$th);
        // }
        
    }

    public function viewInquiryForm()
    {
        $data = Inquiry::all()->sortByDesc('id');
        $FrontPage  = FrontPage::where('slug','inquiry-form')->first();
        return view('admin.cms.inquiryForm.index',compact('data','FrontPage'));
    }

    public function AjaxCallForSaveInquiryFormMetaData(Request $request)
    {
        try {
            $page_detail    = json_encode("There is no data of Inquiry Form in this table.");
            $inquiryForm        = FrontPage::where('slug','inquiry-form')->first();

            $FrontPage                      = ($inquiryForm != null)? FrontPage::find($inquiryForm->id):new FrontPage;
            $FrontPage->title               = $request->title;
            $FrontPage->meta_title          = isset($request->meta_title)? $request->meta_title:'';
            $FrontPage->meta_keywords       = isset($request->meta_keyword)? $request->meta_keyword:'';
            $FrontPage->meta_description    = isset($request->meta_description)? $request->meta_description:'';
            $FrontPage->content             = $page_detail;
            $FrontPage->active              = 1;
            if($inquiryForm == null)
            {
                $FrontPage->slug            = 'inquiry-form'; 
                $FrontPage->created_by      = Auth::user()->id;
                $FrontPage->updated_by      = Auth::user()->id;
            }
            else{
                $FrontPage->updated_by      = Auth::user()->id;
            }
            $FrontPage->save();
            $messege = ($inquiryForm != null)? "Inquiry Form Details Updated Successfully":"Inquiry Form Details Created Successfully";

            return response()->json(['status' => true,'msg' => $messege]);
        } catch (\Throwable $th) {
            return response()->json(['status' => false,'msg' => $th]);

        }
    }

    public function AjaxCallForViewInquiryForm(Request $request)
    {
        $data = Inquiry::find($request->id);
        return view('admin.cms.inquiryForm.viewModal',compact('data'));
    }

    

}
