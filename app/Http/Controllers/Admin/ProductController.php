<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Category;
use Maatwebsite\Excel\Facades\Excel;
use Str;
use File;
use Image;
use Illuminate\Support\Facades\DB;
use App\Imports\ProductImport;


class ProductController extends Controller
{
    public function index()
    {
        $data = Product::all();
        return view('admin.products.index',compact('data'));
    }

    public function create()
    {
        $category = Category::all();
        return view('admin.products.create',compact('category'));
    }

    public function store(Request $request)
    {
        if(isset($request->id))
        {
            $request->validate([
                'name'          => 'required',
                'category_id'   => 'required',
                // 'price'         => 'required|numeric',
                'image'         => 'nullable|image|max:2048',
                'description'   => 'required',
            ]);
            $success_msg    = "Data is Successfully Updated.";
            $product       = Product::find($request->id);

        }
        else{
            
            $request->validate([
                'name'          => 'required',
                'category_id'   => 'required',
                // 'price'         => 'required|numeric',
                'image'         => 'required|image|max:2048',
                'description'   => 'required',
            ]);
            $success_msg    = "Data is Successfully Added.";
            $product       = new Product;
        }

        try{
            

            if($request->hasFile('image'))
            {
                if(isset($request->id))
                {
                    $file           = $product->image;
                    $image_path     = public_path('/backend/images/product').'/'.$file;
                    unlink($image_path);
                }
                $imageName          = time().'.'.$request->image->extension();
                $request->image     ->move(public_path('/backend/images/product'), $imageName);
                $product->image    = $imageName;
            }
            $product->name          = $request->name;
            $product->category_id   = $request->category_id;
            // $product->price         = $request->price;
            $product->description   = $request->description;
            $product->save();

            if($request->hasFile('album'))
            {
                // $imageName = "";
                foreach ($request->file('album') as $index => $photo) 
                {
                    $photoName                  = mt_rand(1000000, 9999999).time().'.'.$photo->extension();
                    $photo                      ->move(public_path('/backend/images/productImage'), $photoName);
                    $ProductImage               = new ProductImage;
                    $ProductImage->product_id   = $product->id;
                    $ProductImage->image        = $photoName;
                    $ProductImage               ->save();
                    // $imageName                  = "";
                }
            }
        }
        catch (\Throwable $th) {
            return redirect()->back()->with('error',$th->getMessage());
        }
        return \redirect()->route('admin.product')->with('success',$success_msg);
    }

    public function edit($id)
    {
        $data = Product::find($id);
        $category = Category::all();
        return view('admin.products.create',compact('category','data'));
    }

    public function destroy($id)
    {
        try {
            $product = Product::find( $id );
            $product->delete();
        } 
        catch (\Throwable $th) 
        {
            return \redirect()->route('admin.product')->with('error',"Record was not deleted!");
        }
        return \redirect()->route('admin.product')->with('success',"Record was deleted successfully!");
    }

    public function AjaxCallForDeleteProductImage(Request $request)
    {
        try {
            $ProductImage = ProductImage::find($request->id);
            $ProductImage->delete();
        } 
        catch (\Throwable $th) 
        {
            return false;
        }
        return true;
    }

    public function importView()
    {
        return view('admin.products.import');
        
    }

    public function importProducts(Request $request)
    {
        // dd($request);

        if($request->hasFile("excel_file"))
        {
            try {
                DB::beginTransaction();
                Excel::import(new ProductImport, $request->excel_file);
                DB::commit();
                return response()->json(["status"=>true,"message"=>"Data Imported Successfully."]);
            } catch (\Throwable $th) {
                DB::rollBack();
                return response()->json(["status"=>false,"message"=>"Excel File is not well formed."]);
            }
        }
    }
}
