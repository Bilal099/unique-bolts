<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use  App\Models\Partner;

class PartnerController extends Controller
{
    public function index()
    {
        $data = Partner::all();
        return view('admin.partners.index',compact('data'));
    }

    public function create()
    {
        return view('admin.partners.create');
    }

    public function store(Request $request)
    {
        if(isset($request->id))
        {
            $request->validate([
                'name'      => 'required',
                'link'      => 'required',
            ]);
            $success_msg    = "Data is Successfully Updated.";
            $partner = Partner::find($request->id);
        }
        else{
            $request->validate([
                'name'      => 'required',
                'image'     => 'required',
                'link'      => 'required',
            ]);
            $success_msg    = "Data is Successfully Added.";
            $partner = new Partner;
        }

        try {
            
            if($request->hasFile('image'))
            {
                if(isset($request->id))
                {
                    $file           = $partner->image;
                    $image_path     = public_path('/backend/images/partner').'/'.$file;
                    unlink($image_path);
                }
                $imageName          = time().'.'.$request->image->extension();
                $request->image     ->move(public_path('/backend/images/partner'), $imageName);
                $partner->image    = $imageName;
            }

            $partner->name  = $request->name;
            $partner->link  = $request->link;
            $partner->save();
        } 
        catch (\Throwable $th) 
        {
            return redirect()->back()->with('error','Some thing is missing!');
        }
        return \redirect()->route('admin.partner')->with('success',$success_msg);

    }

    public function edit($id)
    {
        $data = Partner::find($id);
        return view('admin.partners.create',compact('data'));
    }

    public function destroy(Request $request)
    {
        Partner::findOrFail($request->id)->delete();
        return redirect()->route('admin.partner')->with('success','Record is deleted successfully!');
    }
}
