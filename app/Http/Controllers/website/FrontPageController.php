<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ContactUs;
use App\Models\Inquiry;
use App\Mail\ContactUsEmail;
use App\Mail\ContactUsResponse;
use Illuminate\Support\Facades\Mail;

class FrontPageController extends Controller
{
    public function viewContactUs()
    {
        return view('website.front_pages.contact_us');
    }

    public function AjaxCallForContactUs(Request $request)
    {
        // dd($request->all());
        try {
            $ContactUs          = new ContactUs;
            $ContactUs->name    = $request->name;
            $ContactUs->email   = $request->email;
            $ContactUs->phone   = $request->phone;
            $ContactUs->subject = $request->subject;
            $ContactUs->message = $request->message;
            $ContactUs->save();

            Mail::to(env('CONTACT_US_MAIL'))->send(new ContactUsEmail($ContactUs));

            Mail::to($request->email)->send(new ContactUsResponse($ContactUs));
        } 
        catch (\Throwable $th) 
        {
            return response()->json(['status'=>false, 'message' => "Oops! some issue occur." ]);
        }
        return response()->json(['status'=>true, 'message' => "Your Message is successfully send!" ]);
    }

    public function viewInquiryForm()
    {
        return view('website.front_pages.inquiry_form');
    }

    public function AjaxCallForInquiryForm(Request $request)
    {
        try {
            $Inquiry          = new Inquiry;
            $Inquiry->name    = $request->name;
            $Inquiry->email   = $request->email;
            $Inquiry->subject = $request->subject;
            $Inquiry->message = $request->message;
            $Inquiry->save();
        } 
        catch (\Throwable $th) 
        {
            return response()->json(['status'=>false, 'message' => "Oops! some issue occur." ]);
        }
        return response()->json(['status'=>true, 'message' => "Your Message is successfully send!" ]);
    }


}
