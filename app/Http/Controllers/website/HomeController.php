<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Category;
use App\Models\Product;
use App\Models\FrontPage;
use App\Models\Partner;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $category = Category::all();
        $is_featured = Category::where('featured_category',1)->get();
        // dd(count($is_featured));
        return view('website.front_pages.home',\compact('category','is_featured'));
    }
    public function about_us()
    {
        // dd("adsa");
        $FrontPage  = FrontPage::where('slug','about-us')->first();
        if($FrontPage != null){
            $content    = json_decode($FrontPage->content);
            // dd($content);
        }
        else{
            $content = null;
        }
        return view('website.front_pages.about_us',compact('FrontPage','content'));
    }
    public function shop()
    {
        $category = Category::all();
        $product = Product::all()->sortByDesc('id');
        // dd($product);
        return view('website.front_pages.shop',\compact('category','product'));
    }

    public function sub_category($id)
    {
        $heading = Category::find($id);
        $data =  Category::where('parent_id',$id)->get();
        $product = Product::where('category_id',$id)->get();

        $page = "category";
        return view('website.front_pages.sub_categories_page',\compact('data','page','product','heading'));
    }

    public function productDetails($id)
    {
        $product = Product::where('id',$id)->first();
        return view('website..front_pages.productDetails',compact('product'));
    }

    static public function getPartners()
    {
        $data = Partner::all();

        return $data;
        // dd($data);

    }
}
